﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Day_16.Models;
namespace Day_16.Controllers
{
    public class TryOutsController : Controller
    {
        // GET: TryOuts
        public ActionResult CheckMultipleModels()
        {
            Emp emp = new Emp() { No = 99, Name = "ABC", Address = "Pune" };
            User user = new User() { UserName = "mahesh", Password = "mahesh@123" };

            ViewData["myMessage"] = "Hi from developer";
            ViewBag.loggedInUser = user;
            return View(emp);
        }

        [HttpPost]
        public ActionResult CheckMultipleModels(Emp empModified)
        {
            return null;
        }
    }
}