﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Day_16.Filters;
namespace Day_16.Controllers
{
   
    [LogFilter]
    [Authorize]
    [HandleError(ExceptionType = typeof(SqlException), View = "Error")]
    //[HandleError(ExceptionType = typeof(DivideByZeroException), View = "Error2")]
    //[HandleError(ExceptionType = typeof(ArgumentException), View = "Error3")]
    [HandleError(ExceptionType = typeof(Exception), View = "Error")]
    public class BaseController : Controller
    {
    
    }
}