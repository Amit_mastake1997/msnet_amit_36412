﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Day_16.Models;
using Day_16.Filters;
using System.Data.SqlClient;

namespace Day_16.Controllers
{
    public class HomeController : BaseController
    {
        Karad dbObj = new Karad();

        public ActionResult Index()
        {
            ViewBag.UserName = User.Identity.Name;
            var emps = dbObj.Emps.ToList();
            return View(emps);
        }

        public ActionResult Edit(int id, string FName, string ECity)
        {
            var empToBeUpdated = (from emp in dbObj.Emps
                                  where emp.No == id
                                  select emp).First();
            return View(empToBeUpdated);
        }

        public ActionResult AfterEdit(Emp empUpdated)
        {
            var empToBeUpdated = (from emp in dbObj.Emps
                                  where emp.No == empUpdated.No
                                  select emp).First();
            empToBeUpdated.Name = empUpdated.Name;
            empToBeUpdated.Address = empUpdated.Address;

            dbObj.SaveChanges();

            return Redirect("/Home/Index");
        }

        //public ActionResult AfterEdit(FormCollection entireFormData) 
        //{
        //    int id = Convert.ToInt32(entireFormData["No"]);
        //    string latestname = entireFormData["Name"].ToString();
        //    string latestaddress = entireFormData["Address"].ToString();

        //    var empToBeUpdated = (from emp in dbObj.Emps
        //                          where emp.No == id
        //                          select emp).First();
        //    empToBeUpdated.Name = latestname;
        //    empToBeUpdated.Address = latestaddress;

        //    dbObj.SaveChanges();

        //    return Redirect("/Home/Index");
        //}

        public ActionResult Delete(int id)
        {
            var empToBeDeleted = (from emp in dbObj.Emps
                                  where emp.No == id
                                  select emp).First();
            dbObj.Emps.Remove(empToBeDeleted);
            dbObj.SaveChanges();
            return Redirect("/Home/Index");
        }

        [HttpGet]
        //[AcceptVerbs( HttpVerbs.Get)]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        //[AcceptVerbs(HttpVerbs.POST)]
        public ActionResult Create(Emp emp)
        {
                dbObj.Emps.Add(emp);
                dbObj.SaveChanges();
                return Redirect("/Home/Index");
        }


        [AllowAnonymous]
        public ActionResult About()
        {
            return View();
        }

        //public ActionResult AfterCreate(Emp emp)
        //{
        //    dbObj.Emps.Add(emp);
        //    dbObj.SaveChanges();
        //    return Redirect("/Home/Index");
        //}

        //public ActionResult AfterCreate(Customer c)
        //{

        //    return Redirect("/Home/Index");
        //}

        //public ActionResult AfterCreate(FormCollection entireFormData)
        //{
        //    int id = Convert.ToInt32(entireFormData["No"]);
        //    string name = entireFormData["Name"].ToString();
        //    string address = entireFormData["Address"].ToString();

        //    Emp emp = new Emp() { No = id, Name = name, Address = address };
        //    dbObj.Emps.Add(emp);

        //    dbObj.SaveChanges();

        //    return Redirect("/Home/Index");
        //}
    }

    //public class Customer
    //{
    //    public int No { get; set; }
    //    public string Name { get; set; }
    //    public string Address { get; set; }
    //}
}