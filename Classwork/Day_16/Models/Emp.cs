

namespace Day_16.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Emp
    {
        public int No { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
