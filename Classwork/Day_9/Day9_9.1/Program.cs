﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace _00Demo
{
    delegate int MyDelegate(int x, int y);

    class Program
    {
       
        static void Main(string[] args)
        {
            #region NUllable Type
            /*object obj = null;
            Emp emp = null;

            int? salary = null;

            ////Nullable<int> salary = null;
            if (salary.HasValue)
            {
                Console.WriteLine("You have salary " + salary.ToString());
            }
            else
            {
                Console.WriteLine("Ur salary is yet to be defined!");
            }*/
            #endregion

            #region Anonymous Method
            ////MyDelegate pointer = new MyDelegate(Add);
            ////MyDelegate pointer = new MyDelegate(Method_Name_by_Compiler);


            //MyDelegate pointer = delegate (int x, int y)
            //                        {
            //                            return x + y;
            //                        };
            //int result = pointer(10, 20);
            //Console.WriteLine(result);
            #endregion

            #region Iterator

            //ArrayList arr = new ArrayList();
            //List<string> lst = new List<string>();

            //Week week = new Week();

            //foreach (string day in week)
            //{
            //    Console.WriteLine("Welcome to " + day);
            //}
            #endregion

            #region Implicit Type
            //var a = 99;
            //var v  = 100;  //int v = 100;
            ////v = "abcd";
            ////v = "abcd";
            //v = 200;


            //var v1 = new Emp();   //Emp v1 = new Emp();
            //v1.No = 100;
            //v1.Name = "abcd";

            //Console.WriteLine(v1.GetDetails());


            //var v2 = getMeSomething(10);
            //v2 = 100;
            #endregion

            #region Object Initializer
            //////Emp e1 = new Emp();
            //////e1.No = 1;
            //////e1.Name = "mahesh";
            //////e1.Address = "pune";
            //////_________________
            //////_________________
            //////_________________
            //////_________________
            //////_________________
            //////_________________
            /////

            //Emp e1 = new Emp() { No = 1, Name = "mahesh", Address = "pune" };
            //Console.WriteLine(e1.GetDetails());
            #endregion

            #region Collection Initializer
            //List<Emp> emps = new List<Emp>() {  
            //  new Emp(){ No = 11 , Name = "mahesh1", Address = "pune1" },
            //  new Emp(){ No = 12 , Name = "mahesh2", Address = "pune2" },
            //  new Emp(){ No = 13 , Name = "mahesh3", Address = "pune3" },
            //  new Emp(){ No = 14 , Name = "mahesh4", Address = "pune4" },
            //  new Emp(){ No = 15 , Name = "mahesh5", Address = "pune5" }
            //};

            //foreach (Emp emp in emps)
            //{
            //    Console.WriteLine(emp.GetDetails());
            //}
            #endregion

            #region Anonymous Types
            //var obj = new Emp() { No = 1, Name = "Mahesh", Address = "Pune" };

            //Console.WriteLine(string.Format("No = {0}, Name = {1}, Address = {2}", obj.No, obj.Name, obj.Address));

            //var obj1 = new { No = 1, Name = "Mahesh", Address = "Pune" };
            //var obj2 = new { No = 1, Address = "Pune", Name = "Mahesh" };


            //var obj2 = new { No = 2, Name = 1234, Address = "Panji" };





            //var obj2 = new { No = 2, Name = "Nilesh", Address = "Panji" };



            //var v = (???) obj;

            //Console.WriteLine(string.Format("No = {0}, Name = {1}, Address = {2}", obj.No, obj.Name, obj.Address));

            #endregion

            Emp e = new Emp();
            e.Age = 19;



        }


        //static object getMeSomething(int choice)
        //{
        //    if (choice == 1)
        //    {
        //        return 100;
        //    }
        //    else
        //    {
        //        return new Emp();
        //    }
        //}

        //static int Add(int x, int y)
        //{
        //    return x + y;
        //}
    }

    public class Week : IEnumerable
    {
        private string[] days = new string[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

        public IEnumerator GetEnumerator()
        {
            for (int i = 0; i < days.Length; i++)
            {
                yield return days[i];
            } 
        }
    }

    //public class Emp
    //{
    //    #region Auto Property
    //    public int No { get; set; }
    //    public string Name { get; set; }


    //    #endregion

    //    #region Normal Property

    //    private string _Address;

    //    public string Address
    //    {
    //        get { return _Address; }
    //        set { _Address = value; }
    //    }
    //    #endregion

    //    //private int _No;
    //    //private string _name;

    //    //public string Name
    //    //{
    //    //    get { return _name; }
    //    //    set { _name = value; }
    //    //}

    //    //public int No
    //    //{
    //    //    get { return _No; }
    //    //    set { _No = value; }
    //    //}

    //    public string GetDetails()
    //    {
    //        return string.Format("No = {0}, Name = {1}, Address = {2}", this.No, this.Name, this .Address);
    //    }

    //}
}
