﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00Demo
{

    //Mahesh: Developer
   public partial class Emp
    {
		
		private int _Age;

		partial void Validate(int age);                    //Declaration
		public int Age
		{
			get { return _Age; }
			set 
			{
				Validate(value);
				_Age = value; 
				
			}
		}



	}
}
