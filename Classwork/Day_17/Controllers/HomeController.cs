﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Day_17.Models;
using Day_17.Filters;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace Day_17.Controllers
{
    public class HomeController : BaseController
    {
        Karad dbObj = new Karad();

        [AllowAnonymous]
        public ActionResult Contact()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Contact(FormCollection entireContactForm)
        {
            //Collect Entire Form
            string name = entireContactForm["name"].ToString();
            string email = entireContactForm["email"].ToString();
            string query = entireContactForm["query"].ToString();
            //Send EMail to Admin of the Website

            string adminEMailID = ConfigurationManager.AppSettings["email"].ToString();
            string adminPassword = ConfigurationManager.AppSettings["password"].ToString();

            string smtpDetails = "smtp.gmail.com";
            int smtpPort = 587;

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(adminEMailID);
            mail.To.Add(email);
            mail.CC.Add(adminEMailID);
            mail.Subject = "Query About Your Website!";
            mail.Body = string.Format("<h3>Hello {0}</h3>, <p>We have received your query. We will get back to you soon! </p> <br/> <h4>Details about query received:{1}</h4> <br/><h5>- Regards Support Team</h5> ", name,query);

            mail.IsBodyHtml = true;
            //mail.Attachments.Add()


            SmtpClient smtp = new SmtpClient(smtpDetails, smtpPort);
            smtp.Credentials = new NetworkCredential(adminEMailID, adminPassword);
            smtp.EnableSsl = true;
            smtp.Send(mail);

            //Redirect User to Index Page
            return Redirect("/Home/Index");
        }

        [AllowAnonymous]
        public ActionResult IsNoTaken(int id)
        {
            var empdFound = (from emp in dbObj.Emps.ToList()
                       where emp.No == id
                       select emp).ToList();

            if (empdFound.Count>0)
            {
                return new JsonResult() { Data = true , JsonRequestBehavior = JsonRequestBehavior.AllowGet};
            }
            else
            {
                return new JsonResult() { Data = false, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }
        public ActionResult Index()
        {
            ViewBag.UserName = User.Identity.Name;
            var emps = dbObj.Emps.ToList();
            return View(emps);
        }

        public ActionResult Edit(int id, string FName, string ECity)
        {
            var empToBeUpdated = (from emp in dbObj.Emps
                                  where emp.No == id
                                  select emp).First();
            return View(empToBeUpdated);
        }

        [ValidateAntiForgeryToken]
        public ActionResult AfterEdit(Emp empUpdated)
        {
            var empToBeUpdated = (from emp in dbObj.Emps
                                  where emp.No == empUpdated.No
                                  select emp).First();
            empToBeUpdated.Name = empUpdated.Name;
            empToBeUpdated.Address = empUpdated.Address;

            dbObj.SaveChanges();

            return Redirect("/Home/Index");
        }

        //public ActionResult AfterEdit(FormCollection entireFormData) 
        //{
        //    int id = Convert.ToInt32(entireFormData["No"]);
        //    string latestname = entireFormData["Name"].ToString();
        //    string latestaddress = entireFormData["Address"].ToString();

        //    var empToBeUpdated = (from emp in dbObj.Emps
        //                          where emp.No == id
        //                          select emp).First();
        //    empToBeUpdated.Name = latestname;
        //    empToBeUpdated.Address = latestaddress;

        //    dbObj.SaveChanges();

        //    return Redirect("/Home/Index");
        //}

        public ActionResult Delete(int id)
        {
            var empToBeDeleted = (from emp in dbObj.Emps
                                  where emp.No == id
                                  select emp).First();
            dbObj.Emps.Remove(empToBeDeleted);
            dbObj.SaveChanges();
            return Redirect("/Home/Index");
        }

        [HttpGet]
        //[AcceptVerbs( HttpVerbs.Get)]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        //[AcceptVerbs(HttpVerbs.POST)]
        public ActionResult Create(Emp emp)
        {
                dbObj.Emps.Add(emp);
                dbObj.SaveChanges();
                return Redirect("/Home/Index");
        }


        [AllowAnonymous]
        public ActionResult About()
        {
            return View();
        }

        //public ActionResult AfterCreate(Emp emp)
        //{
        //    dbObj.Emps.Add(emp);
        //    dbObj.SaveChanges();
        //    return Redirect("/Home/Index");
        //}

        //public ActionResult AfterCreate(Customer c)
        //{

        //    return Redirect("/Home/Index");
        //}

        //public ActionResult AfterCreate(FormCollection entireFormData)
        //{
        //    int id = Convert.ToInt32(entireFormData["No"]);
        //    string name = entireFormData["Name"].ToString();
        //    string address = entireFormData["Address"].ToString();

        //    Emp emp = new Emp() { No = id, Name = name, Address = address };
        //    dbObj.Emps.Add(emp);

        //    dbObj.SaveChanges();

        //    return Redirect("/Home/Index");
        //}
    }

    //public class Customer
    //{
    //    public int No { get; set; }
    //    public string Name { get; set; }
    //    public string Address { get; set; }
    //}
}