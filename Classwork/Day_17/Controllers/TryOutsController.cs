﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Day_17.Models;
namespace Day_17.Controllers
{
    
    public class TryOutsController :Controller //BaseController
    {
        // GET: TryOuts
        public ActionResult CheckMultipleModels()
        {
            Emp emp = new Emp() { No = 99, Name = "ABC", Address = "Pune" };
            User user = new User() { UserName = "mahesh", Password = "mahesh@123" };

            ViewData["myMessage"] = "Hi from developer";
            ViewBag.loggedInUser = user;
            return View(emp);
        }

        [HttpPost]
        public ActionResult CheckMultipleModels(Emp empModified)
        {
            return null;
        }

       
        public ActionResult Demo(int id)
        {
            //HandleErrorInfo errorObj = new HandleErrorInfo();
            //errorObj.Exception = new DivideByZeroException() { Message = "Attempted to divide by zero." };
            //errorObj.ControllerName = "TryOuts";
            //errorObj.ActionName = "Demo";

            // on error below call will be given due to [handleError] attribute
            //return View("error", errorObj);
            //int x = id/0;
            return View();
        }

        public ActionResult GetSomething(int id)
        {
            ActionResult result = null;
            switch (id)
            {
                case 1:
                    result =  View("Demo");
                    break;
                case 2:
                    Karad dbObj = new Karad();
                    var allEmpInList = dbObj.Emps.ToList();
                    result = new JsonResult()
                                    {
                                        Data = allEmpInList,
                                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                                    };
                    break;
                case 3:
                    result = new ContentResult()
                                    {
                                        Content = "This is some text ",
                                        ContentType = "text/plain"
                                    };
                    break;
                case 4:
                    result = new JavaScriptResult(){ Script = "<script> alert ('Hi')</script>"};
                    break;
                default:
                    result = null;
                    break;
            }

            return result;
        }
    }
}