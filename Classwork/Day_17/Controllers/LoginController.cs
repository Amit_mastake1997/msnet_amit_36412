﻿using Day_17.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Day_17.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult SignIn()
        {
            //ViewBag.Title = "Sign In Here";
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(User loggedInUser, string ReturnUrl)
        {
            if (this.CheckUserInDB(loggedInUser))
            {
                //user is authenticated .. means user name & password is valid


                //Below code line is creating a session on server 
                //at the same time transferring the cookie to the client side
                FormsAuthentication.SetAuthCookie(loggedInUser.UserName, false); 
                                                //here false means transient cookie.

                if(ReturnUrl!=null)
                {
                    //if user has come to some page / method with [authorize] attribute 
                    //in this case /home/index
                    return Redirect(ReturnUrl);
                }   
                else
                {
                    //if user has come directly to the /Login/SignIn Page
                    return Redirect("/Home/Index");
                }
            }
            else
            {
                //user is not authenticated .. means user name / password is invalid
                //send the same signIn Page back to the user
                ViewBag.ErrorMessage = "UserName / Password is Wrong";
                return View();
            }
            
        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Home/Index");
        }
        private bool CheckUserInDB(User loggedInUser)
        {
            //Write a code w.r.t. SQL Server using connected architecture 
            //to check username and password with some table in db
            //and get true / false returned from the method;

            return (loggedInUser.UserName == "mahesh" && loggedInUser.Password == "mahesh@123");
        }
    }
}