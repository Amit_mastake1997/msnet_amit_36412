﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
namespace _02DemoSerialization
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Serialize Multiple Objects using ArrayList
            //ArrayList arr = new ArrayList();

            //while (true)
            //{
            //    Console.WriteLine("Which object data 1: Emp, 2: Book");
            //    int choice = Convert.ToInt32(Console.ReadLine());
            //    switch (choice)
            //    {
            //        case 1:
            //            Employee emp = new Employee();

            //            Console.WriteLine("Enter No");
            //            emp.No = Convert.ToInt32(Console.ReadLine());

            //            Console.WriteLine("Enter Name");
            //            emp.Name = Console.ReadLine();

            //            Console.WriteLine("Enter Address");
            //            emp.Address = Console.ReadLine();

            //            arr.Add(emp);
            //            break;
            //        case 2:
            //            Book book = new Book();

            //            Console.WriteLine("Enter ISBN No");
            //            book.ISBN = Convert.ToInt32(Console.ReadLine());

            //            Console.WriteLine("Enter Book Title");
            //            book.Title = Console.ReadLine();

            //            arr.Add(book);
            //            break;
            //        default:
            //            Console.WriteLine("Invalid choice!");
            //            break;
            //    }

            //    Console.WriteLine("Do you want to add any more object: 1: yes / 2: no?");
            //    int shallwecontinue = Convert.ToInt32(Console.ReadLine());
            //    if (shallwecontinue == 2)
            //    {
            //        break;
            //    }
            //}

            //FileStream fs = new FileStream(@"E:\DotnetDemos\Day06\Data.txt",
            //                                FileMode.OpenOrCreate, FileAccess.Write);

            //BinaryFormatter writer = new BinaryFormatter();

            //writer.Serialize(fs, arr);
            //writer = null;
            //fs.Close();
            //fs = null;
            #endregion

            #region Deserialize Objects from File - using ArrayList

            FileStream fs = new FileStream(@"E:\DotnetDemos\Day06\Data.txt",
                                            FileMode.Open, FileAccess.Read);

            BinaryFormatter reader = new BinaryFormatter();

            ArrayList arr = (ArrayList)reader.Deserialize(fs);

            foreach (object obj in arr)
            {
                if (obj is Employee)
                {
                    Employee e = (Employee)obj;
                    Console.WriteLine("Welcome " + e.Name);
                }
                else if (obj is Book)
                {
                    Book b = (Book)obj;
                    Console.WriteLine("Nice Book with title : " + b.Title);
                }
                else
                {
                    Console.WriteLine("Unknown object found!!");
                }
            }

            reader = null;
            fs.Close();
            fs = null;

            #endregion
        }
    }



        [Serializable]
        public class Employee
        {
            private int _No;
            private string _Name;
            private string _Address;

            public string Address
            {
                get { return _Address; }
                set { _Address = value; }
            }

            public string Name
            {
                get { return _Name; }
                set { _Name = value; }
            }

            public int No
            {
                get { return _No; }
                set { _No = value; }
            }
        }

        [Serializable]
        public class Book
        {
            private int _ISBNNo;
            private string _Title;

            public string Title
            {
                get { return _Title; }
                set { _Title = value; }
            }

            public int ISBN
            {
                get { return _ISBNNo; }
                set { _ISBNNo = value; }
            }

        }
}
