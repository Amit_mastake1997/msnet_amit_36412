﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _01Demo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            Button button1 = new Button();
            button1.Text = "Click Me";
            button1.Height = 100;
            button1.Width = 200;
            button1.BackColor = Color.BurlyWood;
            button1.ForeColor = Color.White;

            EventHandler fnpointer = new EventHandler(DoSomething);

            button1.Click += fnpointer;

            this.Controls.Add(button1);
        }

        public void DoSomething(object sender, EventArgs e)
        {
            MessageBox.Show("Test");
        }
    }
}
