﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02Demo
{
    //delegate void EventHandler(object sender, EventArgs e);

    delegate int MyDelegate(int x, int y);
    class Program
    {
        static void Main(string[] args)
        {

            #region Direct Call To Method
            //int result = Add(10, 20);
            //Console.WriteLine(result);

            #endregion

            #region InDirect Call To Method
            MyDelegate pointer = new MyDelegate(Add);
            int result = pointer(10, 20);
            Console.WriteLine(result);


            #endregion

            Console.ReadLine();
        }

        static int Add(int x, int y)
        {
            return x + y;
        }
        
    }

   
}
