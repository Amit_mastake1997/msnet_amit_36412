﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_3._2
{
    class Program
    {
        static void Main(string[] args)
        {
            person p1 = new person();
            p1.Name = "Manali";
            p1.Address = "Karad";
            p1.No = 1;
            Console.WriteLine(p1.Name+","+p1.Address+","+p1.No);
            Console.ReadLine();
        }
     
    }
    public class person
    {
        private string name;
        private string address;
        private int no;

        public int No
        {
            get { return this.no; }
            set { this.no = value; }
        }

        public string Address
        {
            get { return this.address; }
            set { this.address = value; }
        }


        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

    }
}
