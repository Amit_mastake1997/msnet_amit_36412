﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_3._3
{
    class Program
    {
        static void Main(string[] args)
        {
            student s1 = new student();
            Console.WriteLine("Enter the roll no:");
            s1.Roll_No = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Name:");
            s1.Name=Console.ReadLine();
            Console.WriteLine("Enter Address:");
            s1.Address=Console.ReadLine();
            Console.WriteLine(s1.Roll_No + "," + s1.Name + "," + s1.Address);
            Console.ReadLine();
        }

        public class student
        {
            private int roll_no;
            private string name;
            private string address;

            public string Address
            {
                get { return address; }
                set { address = value; }
            }

            public string Name
            {
                get { return name; }
                set { name = value; }
            }


            public int Roll_No
            {
                get { return roll_no; }
                set { roll_no = value; }
            }


        }
    }
}
