﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Emp e1 = new Emp();
            e1.Name = "Manali";
            e1.Address = "Karad";
            e1.Empid= 1;
            Console.WriteLine(e1.Name + "," + e1.Address + "," + e1.Empid);
            Console.ReadLine();

        }
    }
    public class Emp
    {
        private string name;
        private string address;
        private int empid;

        public int Empid
        {
            get { return empid; }
            set { empid = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }


        public string Name
        {
            get { return name; }
            set { name = value; }
        }




    }
}
