﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
namespace _01Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Normal File Writing
            //FileStream fs = new FileStream(@"E:\DotnetDemos\Day05\demos\Test.txt", 
            //                                FileMode.OpenOrCreate, 
            //                                FileAccess.Write);

            //StreamWriter writer = new StreamWriter(fs);

            //Console.WriteLine("Enter some statement ");
            //string data = Console.ReadLine();

            //writer.WriteLine(data);

            //writer.Flush();
            //fs.Flush();
            //writer.Close();
            //fs.Close(); 
            #endregion

            #region Normal File Reading

            //if (File.Exists(@"E:\DotnetDemos\Day05\demos\Test.txt"))
            //{
            //    FileStream fs = new FileStream(@"E:\DotnetDemos\Day05\demos\Test.txt",
            //                            FileMode.Open,
            //                            FileAccess.Read);

            //    StreamReader reader = new  StreamReader(fs);

            //    string allDataFromAFile = reader.ReadToEnd();

            //    Console.WriteLine("Data from the file is: " + allDataFromAFile);
            //    reader.Close();
            //    fs.Close();
            //}
            //else
            //{
            //    Console.WriteLine("File Does Not Exist!");
            //}



            #endregion

            #region Writing Object in File
            //FileStream fs = new FileStream(@"E:\DotnetDemos\Day05\demos\Test.txt",
            //                                FileMode.OpenOrCreate,
            //                                FileAccess.Write);

            //BinaryFormatter writer = new BinaryFormatter();

            //Employee employee = new Employee();

            //Console.WriteLine("Enter NO");
            //employee.No = Convert.ToInt32(Console.ReadLine());

            //Console.WriteLine("Enter Name");
            //employee.Name = Console.ReadLine();

            //Console.WriteLine("Enter Address");
            //employee.Address = Console.ReadLine();


            //writer.Serialize(fs,employee);    //This is where we persist employe object on hard drive


            //fs.Flush();

            //writer = null;
            //fs.Close();
            #endregion


            #region Writing Object in File
            FileStream fs = new FileStream(@"E:\DotnetDemos\Day05\demos\Test.txt",
                                            FileMode.Open,
                                            FileAccess.Read);

            BinaryFormatter reader = new BinaryFormatter();


            Employee someEmployeeObject =(Employee) reader.Deserialize(fs);
            Console.WriteLine(someEmployeeObject.Name);


            reader = null;
            fs.Close();
            #endregion
        }
    }

    [Serializable]
    public class Employee
    {
        #region PrivateMembers

        [NonSerialized]
        private string _Password = "abc@123";
        
        
        private int _No;
        private string _Name;
        private string _Address;

        #endregion

        #region Constructors
        public Employee()
        {
            this.No = 0;
            this.Name = "";
            this.Address = "";
        }

        public Employee(int no, string name, string address)
        {
            this.No = no;
            this.Name = name;
            this.Address = address;
        }
        #endregion

        #region Properties
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        #endregion
    }
}
