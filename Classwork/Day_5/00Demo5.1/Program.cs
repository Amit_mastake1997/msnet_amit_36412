﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.CurrentLogger.Log("Main getting called");

            SQLServer s = new SQLServer();
            s.Insert();
        }
    }

    public class SQLServer
    {
       
        public void Insert()
        {
            Console.WriteLine("Insert Query Here....");
            Logger.CurrentLogger.Log("Insert Query is being executed...");
        }
    }

    public class Logger
    {
        private static Logger logger = new Logger();
        private Logger()
        {
            Console.WriteLine("New Logger Object Created");
        }

        public static Logger CurrentLogger
        {
            get
            {
                return logger;
            }
        }
        public void Log(string message)
        {
            //Practically I should write message into DB / File / EMail
            Console.WriteLine("Logged "+ DateTime.Now.ToString() + " " + message); ;
        }
    }
}
