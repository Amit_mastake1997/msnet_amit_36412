﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Day_13._3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        DataTable myTable = new DataTable("Employee");
        DataRow ref_To_Row_With_Specific_PrimaryKey = null;
        SqlConnection con = null;

        DataSet ds =null;

        private void button1_Click(object sender, EventArgs e)
        {
            ds = new DataSet();

            DataColumn col1 = new DataColumn("No", typeof(int));
            DataColumn col2 = new DataColumn("Name", typeof(string));
            DataColumn col3 = new DataColumn("Address", typeof(string));

            myTable.Columns.Add(col1);
            myTable.Columns.Add(col2);
            myTable.Columns.Add(col3);

           myTable.PrimaryKey = new DataColumn[] { myTable.Columns["No"] };

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=Sunbeam;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Emp", con);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DataRow row1 = myTable.NewRow();

                row1["No"] = Convert.ToInt32(reader["No"]);
                row1["Name"] = reader["Name"].ToString();
                row1["Address"] = reader["Address"].ToString();

                myTable.Rows.Add(row1);
            }
            con.Close();

            //DataRow row2 = myTable.NewRow();
            //row2["No"] = 2;
            //row2["Name"] = "Sharddha";
            //row2["Address"] = "Pune";



            //myTable.Rows.Add(row1);

            //myTable.Rows.Add(row2);

            ds.Tables.Add(myTable);
            //dataGridView1.DataSource = myTable;
            //dataGridView1.DataSource = ds.Tables[0];
            dataGridView1.DataSource = ds.Tables["Employee"];
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataRow row3 = myTable.NewRow();
            row3["No"] = 3;
            row3["Name"] = "Sabiya";
            row3["Address"] = "Satara";

            myTable.Rows.Add(row3);

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.ref_To_Row_With_Specific_PrimaryKey = myTable.Rows.Find(1);
            if (this.ref_To_Row_With_Specific_PrimaryKey!=null)
            {

                MessageBox.Show("Record found!!");
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (this.ref_To_Row_With_Specific_PrimaryKey != null)
            {
                this.ref_To_Row_With_Specific_PrimaryKey["Name"] = "Changed";
                this.ref_To_Row_With_Specific_PrimaryKey["Address"] = "Delhi";
            }

            else
            {
                MessageBox.Show("Please Serch the record first!!");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (this.ref_To_Row_With_Specific_PrimaryKey != null)
            {
                this.ref_To_Row_With_Specific_PrimaryKey.Delete();
            }

            else
            {
                MessageBox.Show("Please Serch the record first!!");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DataRow someRow = myTable.Rows.Find(1);
            var status = someRow.RowState;
        }
    }
}
