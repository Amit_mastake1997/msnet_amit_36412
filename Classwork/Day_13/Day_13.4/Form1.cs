﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Day_13._4
{
    public partial class Form1 : Form
    {
        SqlConnection con = null;
        SqlDataAdapter da = null;
        DataSet ds = null;
        SqlCommandBuilder cmb = null;

        DataRow ref_To_Row_With_Specific_PrimaryKey = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=Sunbeam;Integrated Security=True");
            da = new SqlDataAdapter("select * from Emp", con);
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            cmb = new SqlCommandBuilder(da);
            ds = new DataSet();

            da.Fill(ds, "employee");
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataRow row = ds.Tables["employee"].NewRow();
            row["No"] = 7;
            row["Name"] = "Archana";
            row["Address"] = "Karad";

            ds.Tables["employee"].Rows.Add(row);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            da.Update(ds, "employee");
        }

       

        private void button4_Click(object sender, EventArgs e)
        {
            if (ref_To_Row_With_Specific_PrimaryKey == null)
            {
                MessageBox.Show("Please search first");
            }
            else
            {
                ref_To_Row_With_Specific_PrimaryKey["Name"] = "Changed";
                ref_To_Row_With_Specific_PrimaryKey["Address"] = "Changed";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ref_To_Row_With_Specific_PrimaryKey.Delete();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ref_To_Row_With_Specific_PrimaryKey =
                ds.Tables["employee"].Rows.Find(3);

        }
    }
}
