﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Day_13._1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Using Insert Stored Procedure

            //string connectionString = ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
            //SqlConnection connection = new SqlConnection(connectionString);
            //SqlCommand cmd = new SqlCommand("spInsertRecord", connection);
            //cmd.CommandType = CommandType.StoredProcedure;
            //SqlParameter p1 = new SqlParameter("@No", SqlDbType.Int);
            //p1.Value = 8;

            //SqlParameter p2 = new SqlParameter("@name", SqlDbType.VarChar, 50);
            //p2.Value = "Manali";

            //SqlParameter p3 = new SqlParameter("@address", SqlDbType.VarChar, 50);
            //p3.Value = "Karad";

            //cmd.Parameters.Add(p1);
            //cmd.Parameters.Add(p2);
            //cmd.Parameters.Add(p3);

            //connection.Open();

            //int RowsAffected = cmd.ExecuteNonQuery();
            //Console.WriteLine(RowsAffected);

            //Console.WriteLine("Procedure Executed Succesfully");

            #endregion

            #region SQL Injection

            string connectionString = ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
            SqlConnection connection = new SqlConnection(connectionString);
            Console.WriteLine("Enter Your UserName");
            string uname = Console.ReadLine();

            //string query = "select * from Users where UName = '" + uname + "'";
            string query = "select * from Users where UName = @uname";

            SqlCommand cmd = new SqlCommand(query, connection);
            connection.Open();
            SqlParameter p1 = new SqlParameter("@uname", SqlDbType.VarChar, 50);

            p1.Value = uname;

            cmd.Parameters.Add(p1);

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine(reader["uname"].ToString() + reader["password"]);
            }
            Console.ReadLine();

            #endregion
        }
    }
}