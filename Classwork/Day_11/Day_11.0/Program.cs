﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //string path = @"E:\DotnetDemos\Day11\demos\MathsLib\bin\Debug\MathsLib.dll";
            Console.WriteLine("Enter Your Assembly Path:");
            string path = Console.ReadLine();

            Assembly assemblyInfo = Assembly.LoadFrom(path);
            Type[] alltypes = assemblyInfo.GetTypes();
            foreach (Type type in alltypes)
            {
                Console.WriteLine(type.Name);

                //Eg. Below line is going to give is attributes from Maths class 
                var allAttributesOnType = type.GetCustomAttributes();

                bool isTheTypeSerializable = false;

                foreach (var attribute in allAttributesOnType)
                {
                    if (attribute is SerializableAttribute)
                    {
                        isTheTypeSerializable = true;
                        break;
                    }
                }
                if (isTheTypeSerializable == true)
                {
                    Console.WriteLine(" {0} is marked as Serializable",type.Name);
                }
                else
                {
                    Console.WriteLine("{0} is --- NOT marked --- as serializable!", type.Name);
                }

                //FileStream fs = new FileStream("", FileMode.OpenOrCreate, FileAccess.Read | FileAccess.Write);

                MethodInfo[] allMethods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (MethodInfo method in allMethods)
                {
                    Console.Write("--- {0}     {1} ( ", method.ReturnType.ToString() , method.Name );

                    ParameterInfo[] allParameters = method.GetParameters();
                    foreach (ParameterInfo parameter in allParameters)
                    {
                        Console.Write(" {0} {1} " , parameter.ParameterType.ToString(), parameter.Name);
                    }

                    Console.Write(" ) ");
                    Console.WriteLine();
                }
            }

          



        }
    }

    //enum WeekDay
    //{
    //    Monday = 1,
    //    Tuesday = 2,
    //    Wednesday = 3
    //}

    //enum FileMode
    //{
    //    Open = 1,
    //    OpenOrCreate = 2,
    //    Truncate =3 
    //}

}
