﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyOwnAttributes;
namespace DataHolderLib
{
	[Table(TableName="Employee")]
	public class Emp
	{
		[Column(ColumnName="ENo", ColumnType="int", ColumnSize=4)]
		public int No { get; set; }

		[Column(ColumnName = "EName", ColumnType = "varchar", ColumnSize = 50)]
		public string Name { get; set; }

		[Column(ColumnName = "EAddrress", ColumnType = "varchar", ColumnSize = 50)]
		public string Address { get; set; }
	}
}
