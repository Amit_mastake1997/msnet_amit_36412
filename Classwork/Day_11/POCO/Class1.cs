﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyOwnAttributes;
namespace POCO
{

    [Table(TableName = "Department")]
    public class Dept
    {
        [Column(ColumnName = "DeptNo", ColumnSize = 4, ColumnType = "int")]
        public int DNo { get; set; }

        [Column(ColumnName = "DeptName", ColumnSize = 50, ColumnType = "varchar")]
        public string DName { get; set; }
    }
}
