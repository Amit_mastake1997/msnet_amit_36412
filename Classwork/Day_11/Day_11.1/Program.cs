﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
namespace _01Demo
{
    class Program
    {
        static void Main(string[] args)
        {

            string path = @"E:\msnet_amit_36412\Day11\demos\MathsLib\bin\Debug\MathsLib.dll";
            
            Assembly assemblyInfo = Assembly.LoadFrom(path);
            

            Type[] alltypes = assemblyInfo.GetTypes();
            foreach (Type type in alltypes)
            {
                
                Console.WriteLine(type.Name);

                object objectOfSomeType = assemblyInfo.CreateInstance(type.FullName);

                MethodInfo[] allMethods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (MethodInfo method in allMethods)
                {
                    Console.WriteLine("Calling: {0}", method.Name);

                    ParameterInfo[] allParameters = method.GetParameters();

                    object[] parametersToBePassed = new object[allParameters.Length];

                    for (int i = 0; i < allParameters.Length; i++)
                    {
                        Console.WriteLine("Enter the value of {0} type for parameter {1}", 
                                                allParameters[i].ParameterType.ToString(),
                                                allParameters[i].Name);

                        parametersToBePassed[i] = Convert.ChangeType(Console.ReadLine(), allParameters[i].ParameterType);
                    }

                    object result =  
                            type.InvokeMember(method.Name,  //Which  method are we calling?
                            BindingFlags.Public |     //Ensure it is public
                            BindingFlags.Instance |    //Ensure it is available with Instance
                            BindingFlags.InvokeMethod,  //Ensure it is Invokable
                            null,                       //There is no confusion like overloaded / overriden
                            objectOfSomeType,           //which object to use to call the method
                            parametersToBePassed);        //parameters to the method

                    Console.WriteLine("Result is {0}" , result.ToString());
                    Console.WriteLine();
                }
            }





        }
    }
}
