﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyOwnAttributes
{
    public class Table: Attribute
    {
        public string TableName { get; set; }
    }


    public class Column: Attribute
    {
        public string ColumnName { get; set; }
        public string ColumnType { get; set; }
        public int ColumnSize { get; set; }
    }
}
