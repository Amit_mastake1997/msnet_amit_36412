﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using MyOwnAttributes;
namespace _02Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Below is EMP DLL
            //string path = @"E:\msnet_amit_36412\Day11\demos\DataHolderLib\bin\Debug\DataHolderLib.dll";

            Console.WriteLine("Enter POCO DLL Path:");
            string path = Console.ReadLine();

            Assembly assemblyInfo = Assembly.LoadFrom(path);

            Type[] alltypes = assemblyInfo.GetTypes();

            foreach (Type type in alltypes)
            {
                string Query = "create table ";
                IEnumerable<Attribute> allAttributes = type.GetCustomAttributes();

                foreach (Attribute attribute in allAttributes)
                {
                    if (attribute is Table)
                    {
                        Table tableDetails = (Table)attribute;
                        //Console.WriteLine(tableDetails.TableName);
                        Query = Query + tableDetails.TableName + " ( " ;
                        break;
                    }  
                }

                PropertyInfo[] allProperties = type.GetProperties();
                foreach (PropertyInfo property in allProperties)
                {
                    IEnumerable<Attribute> allPropertyAttributes = property.GetCustomAttributes();

                    foreach (Attribute attribute in allPropertyAttributes)
                    {
                        if (attribute is Column)
                        {
                            Column columnDetails = (Column)attribute;
                            Query = Query + columnDetails.ColumnName;
                            Query = Query + "  " + columnDetails.ColumnType;
                            if (columnDetails.ColumnType == "varchar")
                            {
                                Query = Query + "  ( " +  columnDetails.ColumnSize.ToString()  + " ) ";
                            }
                            Query = Query + ",";
                            break;
                            ////Console.WriteLine(columnDetails.ColumnName);
                            ////Console.WriteLine(columnDetails.ColumnType);
                            ////Console.WriteLine(columnDetails.ColumnSize);
                        }
                    }
                    
                }
                Query = Query.TrimEnd(',');
                Query = Query + " ) ";
                Console.WriteLine(Query);

            }

        }
    }
}
