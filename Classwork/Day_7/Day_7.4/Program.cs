﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            //SpecialMaths obj = new SpecialMaths();
            //obj.Swap()

            SpecialMaths<int, short, string, bool> obj = 
                    new SpecialMaths<int, short, string, bool>();

            int result = obj.GetSomeData(100, 1, "mahesh", true);
            Console.WriteLine(result);

            
        }
    }

    public class Maths<T>
    {
        public void Swap(ref T x, ref T y)
        {
            T z;
            z = x;
            x = y;
            y = z;
        }
    }

  

    public class SpecialMaths<p,q,r,s> : Maths<r>  //Generic class inheriting generic class
    {
        public p GetSomeData(p obj1, q obj2, r obj3, s obj4)
        {
            return obj1;
        }
    }

    //public class SpecialMaths : Maths<int>   //Normal class inheriting Generic Class
    //{

    //}
}
