﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region List<T>
            ////List<object> lst = new List<object>();

            //List<Employee> employees = new List<Employee>();

            //while (true)
            //{
            //    Employee  employee = new Employee();
            //    Console.WriteLine("No:");
            //    employee.No = Convert.ToInt32(Console.ReadLine());

            //    Console.WriteLine("Name:");
            //    employee.Name = Console.ReadLine();

            //    Console.WriteLine("Adderss:");
            //    employee.Address = Console.ReadLine();

            //    employees.Add(employee);

            //    Console.WriteLine("Do you want to continue?");
            //    if (Console.ReadLine() != "yes")
            //    {
            //        break;
            //    }
            //}



            //Console.WriteLine("------------ Data Added --------------------");

            //foreach (Employee emp in employees)
            //{
            //    Console.WriteLine(emp.GetDetails()) ;
            //}
            #endregion

            Employee e1 = new Employee(11,"mahesh","pune");
            Employee e2 = new Employee(12, "rahul", "karad");
            Employee e3 = new Employee(13, "akash", "mumbai");
            Employee e4 = new Employee(14, "nilesh", "pune");

            #region Stack<T>
            //Stack<Employee> arr = new Stack<Employee>();
            //arr.Push(e1);
            //arr.Push(e2);
            //arr.Push(e3);
            //arr.Push(e4);

            ////Employee e = arr.Pop();
            ////Console.WriteLine(e.GetDetails());

            //foreach (Employee employee in arr)
            //{
            //    Console.WriteLine(employee.GetDetails());
            //}
            #endregion

            #region Queue<T>
            //Queue<Employee> queue = new Queue<Employee>();
            //queue.Enqueue(e1);
            //queue.Enqueue(e2);
            //queue.Enqueue(e3);
            //queue.Enqueue(e4);


            //Employee e = queue.Dequeue();
            //Console.WriteLine(e.GetDetails()) ;

            //foreach (Employee employee in queue)
            //{
            //    Console.WriteLine(employee.GetDetails());
            //}
            #endregion

            Dictionary<int, Employee> pairs = new Dictionary<int, Employee>();
            pairs.Add(e1.No, e1);
            pairs.Add(e2.No, e2);
            pairs.Add(e3.No, e3);
            pairs.Add(e4.No, e4);

            //Console.WriteLine("Enter the Key against which you would like to an employee's data:");
            //int key = Convert.ToInt32(Console.ReadLine());
            //Employee foundEmp = pairs[key];
            //Console.WriteLine(foundEmp.GetDetails());

            foreach (int key in pairs.Keys)
            {
                Employee emp = pairs[key];
                Console.WriteLine(emp.GetDetails());
            }

            //Dictionary<object, object> arr = new Dictionary<object, object>();

        }
    }

    public class Employee
    {
        private int _No;
        private string _Name;
        private string _Address;

        public Employee(int no, string name, string addres)
        {
            this.No = no;
            this.Name = name;
            this.Address = addres;
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }


        public string GetDetails()
        {
            return string.Format("No = {0}, Name = {1}, Address = {2}", 
                                  this.No, 
                                  this.Name, 
                                  this.Address);
        }
    }
}
