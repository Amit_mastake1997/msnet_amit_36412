﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Maths<int> obj = new Maths<int>();
            int x = 100;
            int y = 200;
            Console.WriteLine(string.Format("Before Swap x={0}, y = {1}", x, y));
            obj.Swap(ref x, ref y);
            Console.WriteLine(string.Format("After Swap x={0}, y = {1}", x, y));

            Maths<string> obj2 = new Maths<string>();
            string s1 = "hello";
            string s2 = "world";
            Console.WriteLine(string.Format("Before Swap s1={0}, s2 = {1}", s1, s2));
            obj2.Swap(ref s1, ref s2);
            Console.WriteLine(string.Format("After Swap s1={0}, s2 = {1}", s1, s2));

        }
    }

    public class Maths<T>  //Generic Class
    {
        public void Swap(ref T x, ref T y)  //Generic Method
        {
            T z;
            z = x;
            x = y;
            y = z;
        }

        public int Add(int x, int y)        //Normal nonGeneric Method
        {
            return x + y;
        }
        public int Sub(int x, int y)       //Normal nonGeneric Method 
        {
            return x - y;
        }
    }

}
