﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02Demo
{
    class Program
    {
        static void Main(string[] args)
        {

            Maths obj = new Maths();
            string s1 = "hello";
            string s2 = "world";
            Console.WriteLine(string.Format("Before Swap s1={0}, s2 = {1}", s1, s2));
            obj.Swap<string>(ref s1, ref s2);
            Console.WriteLine(string.Format("After Swap s1={0}, s2 = {1}", s1, s2));

            int result = obj.Add(10, 20);
            Console.WriteLine(result);

        }
    }

    public class Maths                              //Normal Non Generic Class
    {
        public void Swap<T>(ref T x, ref T y)       //Generic Method
        {
            T z;
            z = x;
            x = y;
            y = z;
        }

        public int Add(int x, int y)                //Normal nonGeneric Method
        {
            return x + y;
        }
        public int Sub(int x, int y)                //Normal nonGeneric Method
        {
            return x - y;
        }
    }

}
