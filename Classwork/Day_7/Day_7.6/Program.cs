﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
namespace _05Demo
{
    class Program
    {
        static void Main(string[] args)
        {

            //Employee e1 = new Employee(11, "mahesh", "pune");
            //Employee e2 = new Employee(12, "rahul", "karad");
            //Employee e3 = new Employee(13, "akash", "mumbai");
            //Employee e4 = new Employee(14, "nilesh", "pune");

            //List<Employee> employees = new List<Employee>();
            //employees.Add(e1);
            //employees.Add(e2);
            //employees.Add(e3);
            //employees.Add(e4);

            #region Single Object XML Serialization
            //FileStream fs = new FileStream(@"E:\DotnetDemos\Day07\data.xml",
            //                                FileMode.OpenOrCreate,
            //                                FileAccess.Write);

            //XmlSerializer writer = new XmlSerializer(typeof(Employee));
            //writer.Serialize(fs, e1);
            //fs.Flush();
            //writer = null;
            //fs = null;

            #endregion

            #region Single Object XML DeSerialization 

            //FileStream fs = new FileStream(@"E:\DotnetDemos\Day07\data.xml",
            //                                FileMode.Open,
            //                                FileAccess.Read);

            //XmlSerializer reader = new XmlSerializer(typeof(Employee));
            //Employee emp = (Employee) reader.Deserialize(fs);
            //Console.WriteLine(emp.GetDetails());

            //reader = null;
            //fs = null;
            #endregion

            #region Employee Collection XML Serialization
            //FileStream fs = new FileStream(@"E:\DotnetDemos\Day07\data.xml",
            //                                FileMode.OpenOrCreate,
            //                                FileAccess.Write);

            //XmlSerializer writer = new XmlSerializer(typeof(List<Employee>));
            //writer.Serialize(fs, employees);
            //fs.Flush();
            //writer = null;
            //fs = null;

            #endregion

            #region Employee Collection DeSerialization

            //FileStream fs = new FileStream(@"E:\DotnetDemos\Day07\data.xml",
            //                                FileMode.Open,
            //                                FileAccess.Read);

            //XmlSerializer reader = new XmlSerializer(typeof(List<Employee>));

            //List<Employee> lst   = (List<Employee>)reader.Deserialize(fs);

            //foreach (Employee emp in lst)
            //{
            //    Console.WriteLine(emp.GetDetails());
            //}

            //reader = null;
            //fs = null;
            #endregion
        }
    }

    public class Employee
    {
        private int _No;
        private string _Name;
        private string _Address;

        public Employee()
        {

        }
        public Employee(int no, string name, string addres)
        {
            this.No = no;
            this.Name = name;
            this.Address = addres;
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }


        public string GetDetails()
        {
            return string.Format("No = {0}, Name = {1}, Address = {2}",
                                  this.No,
                                  this.Name,
                                  this.Address);
        }
    }
}
