﻿using _00Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _00Demo.Controllers
{
    public class TestController : Controller
    {

        Karad dbObj = new Karad();
        public ActionResult Index()
        {
            var emps = dbObj.Emps.ToList();
            return View(emps);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Emp emp)
        {
            try
            {
                dbObj.Emps.Add(emp);
                dbObj.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult Edit(int id)
        {
            var empToBeUpdated = (from emp in dbObj.Emps
                                  where emp.No == id
                                  select emp).First();
            return View(empToBeUpdated);
        }

        [HttpPost]
        public ActionResult Edit(int id, Emp empUpdated)
        {
            try
            {
                var empToBeUpdated = (from emp in dbObj.Emps
                                      where emp.No == empUpdated.No
                                      select emp).First();
                empToBeUpdated.Name = empUpdated.Name;
                empToBeUpdated.Address = empUpdated.Address;

                dbObj.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            var empToBeDeleted = (from emp in dbObj.Emps
                                  where emp.No == id
                                  select emp).First();
            dbObj.Emps.Remove(empToBeDeleted);
            dbObj.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}
