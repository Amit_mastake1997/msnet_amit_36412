﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace _01Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString =
                @"server=(LocalDB)\MSSQLLocalDB; database=Karad; integrated security=true";

            // string connectionString =
            //"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=Karad;Integrated Security=True"

            #region Select Query on SQL Server Database
            //SqlConnection connection = new SqlConnection(connectionString);
            //connection.Open();
            //SqlCommand command = new SqlCommand("select * from Emp", connection);
            //SqlDataReader reader = command.ExecuteReader();

            //while (reader.Read())
            //{
            //    Console.WriteLine(reader[0].ToString() + " | " + reader["Name"] + " | " + reader[2].ToString());
            //}

            //connection.Close();
            #endregion


            #region Insert Query on SQL Server Database : Hard coded data
            //SqlConnection connection = new SqlConnection(connectionString);
            //connection.Open();
            //SqlCommand command = new SqlCommand("insert into Emp values(4,'Akash', 'Mumbai')", connection);
            //int RowsAffected = command.ExecuteNonQuery();
            //Console.WriteLine("No of rows affected = {0}", RowsAffected);
            //connection.Close();
            #endregion

            #region Insert Query on SQL Server Database : Values collected from User
            //try
            //{
            //    Console.WriteLine("Enter No");
            //    int no = Convert.ToInt32(Console.ReadLine());

            //    Console.WriteLine("Enter Name");
            //    string name = Console.ReadLine();

            //    Console.WriteLine("Enter Address");
            //    string address = Console.ReadLine();

            //    string queryFormat = "insert into Emp values({0},'{1}', '{2}')";
            //    string finalQuery = string.Format(queryFormat, no, name, address);

            //    SqlConnection connection = new SqlConnection(connectionString);
            //    connection.Open();
            //    SqlCommand command = new SqlCommand(finalQuery, connection);
            //    int RowsAffected = command.ExecuteNonQuery();
            //    Console.WriteLine("No of rows affected = {0}", RowsAffected);
            //    connection.Close();
            //}
            //catch(Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //Console.ReadLine();
            #endregion

            #region Insert in Specific Columns
            //try
            //{
            //    Console.WriteLine("Enter No");
            //    int no = Convert.ToInt32(Console.ReadLine());

            //    Console.WriteLine("Enter Name");
            //    string name = Console.ReadLine();

            //    string queryFormat = "insert into Emp(No,Name) values({0},'{1}')";
            //    string finalQuery = string.Format(queryFormat, no, name);

            //    SqlConnection connection = new SqlConnection(connectionString);
            //    connection.Open();
            //    SqlCommand command = new SqlCommand(finalQuery, connection);
            //    int RowsAffected = command.ExecuteNonQuery();
            //    Console.WriteLine("No of rows affected = {0}", RowsAffected);
            //    connection.Close();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //Console.ReadLine();
            #endregion

            #region Update Query on SQL Server Database : Values collected from user

            //try
            //{
            //    Console.WriteLine("Enter No of the Employee whose record needs to updated:");
            //    int no = Convert.ToInt32(Console.ReadLine());

            //    Console.WriteLine("Enter New Name");
            //    string name = Console.ReadLine();

            //    Console.WriteLine("Enter New Address");
            //    string address = Console.ReadLine();

            //    string queryFormat = "update Emp set Name='{1}', Address='{2}' where No = {0}";
            //    string finalQuery = string.Format(queryFormat, no, name, address);

            //    SqlConnection connection = new SqlConnection(connectionString);
            //    connection.Open();
            //    SqlCommand command = new SqlCommand(finalQuery, connection);
            //    int RowsAffected = command.ExecuteNonQuery();
            //    Console.WriteLine("No of rows affected = {0}", RowsAffected);
            //    connection.Close();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //Console.ReadLine();
            #endregion

            #region Delete Query on SQL Server Database : Values collected from user

            //try
            //{
            //    Console.WriteLine("Enter No of the Employee whose record needs to Deleted:");
            //    int no = Convert.ToInt32(Console.ReadLine());

            //    string queryFormat = "Delete from Emp where No = {0}";
            //    string finalQuery = string.Format(queryFormat, no);

            //    SqlConnection connection = new SqlConnection(connectionString);
            //    connection.Open();
            //    SqlCommand command = new SqlCommand(finalQuery, connection);
            //    int RowsAffected = command.ExecuteNonQuery();
            //    Console.WriteLine("No of rows affected = {0}", RowsAffected);
            //    connection.Close();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //Console.ReadLine();
            #endregion

            #region Insert in Specific Columns in case of Auto Increment
            //try
            //{
                
            //    Console.WriteLine("Enter Name");
            //    string name = Console.ReadLine();

            //    Console.WriteLine("Enter Address");
            //    string address = Console.ReadLine();

            //    string queryFormat = "insert into Customer(Name,Address) values('{0}','{1}')";
            //    string finalQuery = string.Format(queryFormat, name, address);

            //    SqlConnection connection = new SqlConnection(connectionString);
            //    connection.Open();
            //    SqlCommand command = new SqlCommand(finalQuery, connection);
            //    int RowsAffected = command.ExecuteNonQuery();
            //    Console.WriteLine("No of rows affected = {0}", RowsAffected);
            //    connection.Close();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //Console.ReadLine();
            #endregion
        }
    }
}
