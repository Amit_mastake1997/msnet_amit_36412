﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Dynamic Type
            //Console.WriteLine("Enter choice");
            //int choice = Convert.ToInt32(Console.ReadLine());

            //dynamic obj = GetMeSomeObject(choice);
            //Console.WriteLine(obj.getdetails());
            //object obj = GetMeSomeObject(choice);

            //if (obj is Emp)
            //{
            //    Emp emp = (Emp)obj;
            //    Console.WriteLine(emp.getDetails());
            //}
            //else if (obj is Book)
            //{
            //    Book book = (Book)obj;
            //    Console.WriteLine(book.getBookDetails());
            //}
            #endregion

            #region Optional & Named Parameters
            //Emp e = new Emp();
            //string data = e.getDetails(1,address:"chennai", name : "mahesh");
            //Console.WriteLine(data);
            #endregion

        }

        static object GetMeSomeObject(int choice)
        {
            if (choice == 1)
            {
                return new Emp();
            }
            else
            {
                return new Book();
            }
        }
    }

    public class Emp
    {
        public string getDetails(int no, string name="sachin", string address="Pune")
        {
            return string.Format("No = {0}, Name = {1}, Address= {2}", no, name, address);
        }

        //public string getDetails()
        //{
        //    return "Some EMP Details";
        //}
    }

    public class Book
    {
        public string getBookDetails()
        {
            return "Some BOOK Details";
        }
    }
}
