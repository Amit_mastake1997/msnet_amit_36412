﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DBCode;
namespace _02Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString  = 
                @"server=(LocalDB)\MSSQLLocalDB; database=Karad; integrated security=true";

            SQLServer dbObj = new SQLServer(connectionString);

            int rowsInserted = dbObj.Insert(new Emp() { No = 8, Name = "Vivek", Address = "Banglore" });

            Console.WriteLine("No of rows Inserted = {0}", rowsInserted);


            List<Emp> allEmps = dbObj.Select();

            foreach (Emp emp in allEmps)
            {
                Console.WriteLine(emp.Name);
            }
            Console.ReadLine();


            Console.ReadLine();
        }
    }
}
