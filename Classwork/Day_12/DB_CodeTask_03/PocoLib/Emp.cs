﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PocoLib
{
    public class Emp
    {
            public int EmpNo { get; set; }
            public string EName { get; set; }
            public string Designation { get; set; }
            public int Salary { get; set; }
            public string HireDate { get; set; }
    }
}
