﻿using DataAccessLayerLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PocoLib;

namespace HRApp
{
    class Program
    {
       

        public static int MenuList()
        {
            int choice;
            Console.WriteLine("0. Exit");
            Console.WriteLine("1. Insert Emp ");
            Console.WriteLine("2. Insert Dept");
            Console.WriteLine("3. Update Emp");
            Console.WriteLine("4. Update Dept ");
            Console.WriteLine("5. Delete emp");
            Console.WriteLine("6. Delete Dept");
            Console.WriteLine("7. Select All");
            Console.WriteLine("Enter choice : ");
            choice = Convert.ToInt32(Console.ReadLine());
            return choice;
        }
        static void Main(string[] args)
        {
             string connectionString =
               @"server=(LocalDB)\MSSQLLocalDB; database=MYDB2; integrated security=true";

            DeptDAL dbObj = new DeptDAL(connectionString);
            EmpDAL dbObj1 = new EmpDAL(connectionString);

            int choice;
          
            while ((choice = MenuList()) != 0)
            {
                switch (choice)
                {
                    case 1:
                        int rowsInserted = dbObj1.Insert(new Emp());

                        Console.WriteLine("No of rows Inserted = {0}", rowsInserted);

                        break;
                    case 2:
                        int rowsInserted4 = dbObj.Insert(new Dept());

                        Console.WriteLine("No of rows Inserted = {0}", rowsInserted4);
                        break;
                    case 3:
                        int rowsInserted2 = dbObj1.Update(new Emp());

                        Console.WriteLine("No of rows Inserted = {0}", rowsInserted2);
                        break;
                    case 4:
                        int rowsInserted5 = dbObj.Update(new Dept());

                        Console.WriteLine("No of rows Inserted = {0}", rowsInserted5);
                        break;
                    case 5:
                        Console.WriteLine("Enter No");
                        int id = Convert.ToInt32(Console.ReadLine());

                        int rowsInserted1 = dbObj1.Delete(id);

                        Console.WriteLine("No of rows Inserted = {0}", rowsInserted1);
                        break;
                    case 6:
                        Console.WriteLine("Enter No");
                        int no = Convert.ToInt32(Console.ReadLine());
                        int rowsInserted23 = dbObj.Delete(no);
                        Console.WriteLine("No of rows Inserted = {0}", rowsInserted23);

                        break;
                }


            }
            Console.ReadLine();
        }
    }
}
