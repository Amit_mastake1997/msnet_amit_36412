﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PocoLib;
namespace DataAccessLayerLib
{
    public class EmpDAL
    {
        private string connectionString = "";

        public EmpDAL(string connectionstring)
        {
            this.connectionString = connectionstring;
        }
        public int Insert(Emp emp)
        {
        
                Console.WriteLine("Enter Name");
              string name = Console.ReadLine();

            Console.WriteLine("Enter Designation");
                string Designation = Console.ReadLine();
            Console.WriteLine("Enter Salary");
            int Salary = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter HireDate");
            string HireDate = Console.ReadLine();

            emp.EName = name;
            emp.Designation = Designation;
            emp.Salary = Salary;
            emp.HireDate = HireDate;
            string queryFormat = "insert into Emp values('{0}','{1}',{2},'{3}')";
            string finalQuery = string.Format(queryFormat, emp.EName,emp.Designation,emp.Salary,emp.HireDate);

            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand(finalQuery, connection);
            int RowsAffected = command.ExecuteNonQuery();
            connection.Close();

            return RowsAffected;
        }
        public int Update(Emp emp)
        {
            Console.WriteLine("Enter No");
            int no = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Name");
            string name = Console.ReadLine();

            Console.WriteLine("Enter Designation");
            string Designation = Console.ReadLine();
            Console.WriteLine("Enter Salary");
            int Salary = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter HireDate");
            string HireDate = Console.ReadLine();
            emp.EmpNo = no;
            emp.EName = name;
            emp.Designation = Designation;
            emp.Salary = Salary;
            emp.HireDate = HireDate;

            string queryFormat = "update Emp set EName='{1}', Designation='{2}', Salary='{3}', HireDate='{4}' where EmpNo = {0}";
            string finalQuery = string.Format(queryFormat, emp.EmpNo, emp.EName, emp.Designation,emp.Salary,emp.HireDate);

            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand(finalQuery, connection);
            int RowsAffected = command.ExecuteNonQuery();
            connection.Close();
            return RowsAffected;
        }
        public int Delete(int id)
        {
            Emp emp=new Emp();

           
            emp.EmpNo = id;
           
            string queryFormat = "Delete from Emp where EmpNo = {0}";
            string finalQuery = string.Format(queryFormat, id);

            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand(finalQuery, connection);
            int RowsAffected = command.ExecuteNonQuery();
            connection.Close();
            return RowsAffected;
        }
    }
}
