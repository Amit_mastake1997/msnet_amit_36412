﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PocoLib;

namespace DataAccessLayerLib
{
    public class DeptDAL
    {
        private string connectionString = "";

        public DeptDAL(string connectionstring)
        {
            this.connectionString = connectionstring;
        }
        public int Insert(Dept dept)
        {
            

            Console.WriteLine("Enter Name");
            string name = Console.ReadLine();

            Console.WriteLine("Enter Location");
            string Location = Console.ReadLine();

            dept.DName = name;
            dept.Location = Location;

            string queryFormat = "insert into Dept values('{0}', '{1}')";
            string finalQuery = string.Format(queryFormat,dept.DName,dept.Location);

           SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand(finalQuery, connection);
            int RowsAffected = command.ExecuteNonQuery();
            connection.Close();

            return RowsAffected;
        }
        public int Update(Dept dept)
        {
            Console.WriteLine("Enter No");
            int no = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Name");
            string name = Console.ReadLine();

            Console.WriteLine("Enter Location");
            string Location = Console.ReadLine();
            dept.DeptNo = no;
            dept.DName = name;
            dept.Location = Location;

            string queryFormat = "update Dept set DName='{1}', Location='{2}' where DeptNo = {0}";
            string finalQuery = string.Format(queryFormat,dept.DeptNo,dept.DName,dept.Location);

            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand(finalQuery, connection);
            int RowsAffected = command.ExecuteNonQuery();
            connection.Close();
            return RowsAffected;
        }
        public int Delete(int id)
        {

          
            Dept dept = new Dept();
            dept.DeptNo = id;
            string queryFormat = "Delete from Dept where DeptNo = {0}";
            string finalQuery = string.Format(queryFormat, id);

            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand(finalQuery, connection);
            int RowsAffected = command.ExecuteNonQuery();
            connection.Close();
            return RowsAffected;
        }
    }
}
