﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_4._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee p = new Employee();

            Console.WriteLine("Enter No");
            p.No = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Name");
            p.Name = Console.ReadLine();

            Console.WriteLine("Enter Address");
            p.Address = Console.ReadLine();

            Console.WriteLine("You are working! true / false?");
            p.IsWorking = Convert.ToBoolean(Console.ReadLine());

            Console.WriteLine("Enter Department");
            p.Dept = Console.ReadLine();


            Console.WriteLine("You entered:");
            string details = p.GetDetails();

            Console.WriteLine(details);

            Console.ReadLine();
        }
    }

    public class Person
    {
        

        private int no;
        private string name;
        private string address;
        private bool isworking;


        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public bool IsWorking
        {
            get { return isworking; }
            set { isworking = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

      


        public int No
        {
            get { return no; }
            set { no = value; }
        }




        public virtual string GetDetails()
        {
            return this.No.ToString() + this.Name + this.Address + this.IsWorking.ToString();
        }

    }

    public class Employee : Person
    {
        private string dept;

        public string Dept
        {
            get { return dept; }
            set { dept = value; }
        }

        public override string GetDetails()
        {
            return this.No.ToString() + this.Name + this.Address + this.IsWorking.ToString() + this.Dept;
        }

    }
}