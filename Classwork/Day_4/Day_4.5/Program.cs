﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_4._5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Database obj = new Oracle();
            Database obj = new SQLServer();


            obj.Insert();
        }
    }

    public interface Database
    {
        void Insert();
        void Update();
        void Delete();
    }

    public class SQLServer : Database
    {
        public void Insert()
        {
            Console.WriteLine("Data Inserted into SQL Server DB");
        }
        public void Update()
        {
            Console.WriteLine("Data Updated into SQL Server DB");
        }
        public void Delete()
        {
            Console.WriteLine("Data Deleted from SQL Server DB");
        }
    }


    public class Oracle : Database
    {
        public void Insert()
        {
            Console.WriteLine("Data Inserted into Oracle Server DB");
        }
        public void Update()
        {
            Console.WriteLine("Data Updated into Oracle Server DB");
        }
        public void Delete()
        {
            Console.WriteLine("Data Deleted from Oracle Server DB");
        }
    }
}
