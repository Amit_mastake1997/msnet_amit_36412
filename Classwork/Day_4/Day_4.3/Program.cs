﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_4._3
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("\n1: PDF\n2: DOCX \n3: PPT \n4: Excel \n5: Text");
            int choice = Convert.ToInt32(Console.ReadLine());

    
            Report report = ReportFactory.GetReport(choice); 

            
            report.Execute();

            Console.ReadLine();
        }
    }
    public class ReportFactory
    {
        public static Report GetReport(int choice)
        {
            if (choice == 1)
            {
                return new PDF();
            }

            else if (choice == 2)
            {
                return new DOCX();
            }
            else if (choice == 3)
            {
                return new PPT();
            }
            else if (choice == 4)
            {
                return new Excel();
            }
            else
            {
                return new Text();
            }

        }
    }      
    public abstract class Report
    {
        protected abstract void Read();
        protected abstract void Parse();
        protected abstract void Validate();
        protected abstract void Print();

        public virtual void Execute()
        {
            Read();
            Parse();
            Validate();
            Print();
        }
    }   
    public abstract class SpecialReport : Report
    {
        protected abstract void ReValidate();
        public override void Execute()
        {
            Read();
            Parse();
            Validate();
            ReValidate();
            Print();
        }
    }  
    public class PDF : Report
    {
        protected override void Read()
        {
           
            Console.WriteLine("Read PDF");
        }
        protected override void Parse()
        {
           
            Console.WriteLine("Parsing PDF");
        }
        protected override void Validate()
        {
          
            Console.WriteLine("Validating PDF");
        }
        protected override void Print()
        { 
            Console.WriteLine("PDF Getting Printed Here...");
        }
    }               
    public class DOCX : Report
    {
        protected override void Read()
        {
            
            Console.WriteLine("Read DOCX");
        }
        protected override void Parse()
        {
            
            Console.WriteLine("Parsing DOCX");
        }
        protected override void Validate()
        {
            
            Console.WriteLine("Validating DOCX");
        }
        protected override void Print()
        {
            
            Console.WriteLine("DOCX Getting Printed Here...");
        }
    }             
    public class PPT : Report
    {
        protected override void Parse()
        {
            Console.WriteLine("PPT Parsed");
        }
        protected override void Print()
        {
            Console.WriteLine("PPT Printed");
        }

        protected override void Read()
        {
            Console.WriteLine("PPT Read");
        }

        protected override void Validate()
        {
            Console.WriteLine("PPT Validated");
        }
    }              
    public class Excel : SpecialReport
    {
        protected override void Parse()
        {
            Console.WriteLine("Excel Parsed");
        }

        protected override void Print()
        {
            Console.WriteLine("Excel Printed");
        }

        protected override void Read()
        {
            Console.WriteLine("Excel Read");
        }

        protected override void Validate()
        {
            Console.WriteLine("Excel Validated");
        }

        protected override void ReValidate()
        {
            Console.WriteLine("Excel ReValidated");
        }


    }            
    public class Text : SpecialReport
    {
        protected override void Parse()
        {
            Console.WriteLine("TXT Parsed");
        }

        protected override void Print()
        {
            Console.WriteLine("TXT Printed");
        }

        protected override void Read()
        {
            Console.WriteLine("TXT Read");
        }

        protected override void Validate()
        {
            Console.WriteLine("TXT Validated");
        }

        protected override void ReValidate()
        {
            Console.WriteLine("TXT ReValidated");
        }


    }             
}
