﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_4._2
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Whose details you would like to enter \n1: Employee \n2: Customer");
            int choice = Convert.ToInt32(Console.ReadLine());

        
            Person p = PersonSelector.GetSomePerson(choice);

            Console.WriteLine("You entered:");
            string details = p.GetDetails();

            Console.WriteLine(details);

            Console.ReadLine();
        }
    }

    public class Person
    {
        private int no;
        private string name;
        private string address;
        private bool isworking;


        public bool IsWorking
        {
            get { return isworking; }
            set { isworking = value; }
        }


        public string Address
        {
            get { return address; }
            set { address = value; }
        }


        public string  Name
        {
            get { return name; }
            set { name = value; }
        }


        public int No
        {
            get { return no; }
            set { no = value; }
        }



        public virtual string GetDetails()
        {
            return this.No.ToString()+" " + this.Name+" " + this.Address+" " + this.IsWorking.ToString();
        }

    }

    public class Employee : Person
    {
        private string dept;

        public string Dept
        {
            get { return dept; }
            set { dept = value; }
        }



        public override string GetDetails()
        {
            return this.No.ToString()+" " + this.Name+" " + this.Address+"  " + this.IsWorking.ToString()+" " + this.Dept;
        }

    }

    public class Customer : Person
    {
        private string _OrderDesc;

        public string OrderDesc
        {
            get { return _OrderDesc; }
            set { _OrderDesc = value; }
        }
        public override string GetDetails()
        {
            return this.No.ToString() + this.Name + this.Address + this.IsWorking.ToString() + this.OrderDesc;
        }
    }

    public class PersonSelector                   
    {
        public static Person GetSomePerson(int choice)  
        {
            if (choice == 1)
            {
                Employee e = new Employee();

                Console.WriteLine("Enter No");
                e.No = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter Name");
                e.Name = Console.ReadLine();

                Console.WriteLine("Enter Address");
                e.Address = Console.ReadLine();

                Console.WriteLine("You are working! true / false?");
                e.IsWorking = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine("Enter Department");
                e.Dept = Console.ReadLine();

                return e;
            }
            else
            {
                Customer c = new Customer();

                Console.WriteLine("Enter No");
                c.No = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter Name");
                c.Name = Console.ReadLine();

                Console.WriteLine("Enter Address");
                c.Address = Console.ReadLine();

                Console.WriteLine("You are working! true / false?");
                c.IsWorking = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine("Enter Order Desc");
                c.OrderDesc = Console.ReadLine();

                return c;
            }
        }
    }
}
