﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_4._4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Department department = new Department();
            //department.DeptNo = 10;
            //department.DeptName = "Admin";

            Employee employee = new Employee();
            employee.No = 1;
            employee.Name = "Manali";
            //employee.EDepartment = department;

            employee.PrintDetails();
            Console.ReadLine();
        }
    }

    public class Employee
    {
        
        private int no;
        private string name;
        private Department _EDepartment;



        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        public int No
        {
            get { return no; }
            set { no = value; }
        }


        public Department EDepartment
        {
            get { return _EDepartment; }
            set { _EDepartment = value; }
        }

       

        public void PrintDetails()
        {
            Console.WriteLine(this.No.ToString() + "   " + this.Name + "  " + this.EDepartment.DeptNo + this.EDepartment.DeptName);
        }
    }

    public class Department
    {
        private int deptno;
        private string deptname;

        public string DeptName
        {
            get { return deptname; }
            set { deptname = value; }
        }


        public int DeptNo
        {
            get { return deptno; }
            set { deptno = value; }
        }


    }
}

