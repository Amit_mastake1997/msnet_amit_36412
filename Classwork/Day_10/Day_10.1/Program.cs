﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00Demo
{
    class Program
    {
        //delegate int MyDelegate(int x, int y);

        //delegate R MyDelegate<P,Q,R>(P x, Q y);

        static void Main(string[] args)
        {
            #region Anonymous Collection
            //var arr = new[] { 
            //    new { no =11, name = "abc1", address="pune1" },
            //    new { no =12, name = "abc2", address="pune2" },
            //    new { no =13, name = "abc3", address="pune3" },
            //    new { no =14, name = "abc4", address="pune4" },
            //    new { no =15, name = "abc5", address="pune5" }
            //};


            //foreach (var item in arr)
            //{
            //    Console.WriteLine(item.name);
            //}

            //var lst = arr.ToList();
            #endregion

            //int result = Add(10, 20);
            //Console.WriteLine(result);

            //MyDelegate pointer = new MyDelegate(Add);
            //int result = pointer(10, 20);
            //Console.WriteLine(result);

            //MyDelegate pointer =  delegate (int x, int y)
            //                        {
            //                            return x + y;
            //                        };
            //int result = pointer(10, 20);
            //Console.WriteLine(result);


            //MyDelegate pointer =  (x,y)=>
            //                        {
            //                            return x + y;
            //                        };
            //int result = pointer(10, 20);
            //Console.WriteLine(result);

            //Func<int,int, int> pointer = delegate (int x, int y)
            //                                {
            //                                    return x + y;
            //                                };
            //int result = pointer(10, 20);
            //Console.WriteLine(result);


            //Func<int, int, int> pointer = delegate (int x, int y)
            //                                      {
            //                                          return x + y;
            //                                      };
            //int result = pointer(10, 20);
            //Console.WriteLine(result);



            //Func<int, int, int> pointer =  (x,y)=>
            //                                {
            //                                    return x + y;
            //                                };
            //int result = pointer(10, 20);
            //Console.WriteLine(result);

        }


        


        //static int Add(int x, int y)
        //{
        //    return x + y;
        //}
    }
}
