﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10
{
    class Program
    {
        static string cityDetails = "";
        static void Main(string[] args)
        {
            #region Normal Filtering of Collection using For Each + if
            //List<Emp> emps = GetAllEmployees();

            //Console.WriteLine("Enter City Details to Find the Employee:");

            //string cityDetails = Console.ReadLine();

            //List<Emp> searchResult = new List<Emp>();

            //foreach (Emp emp in emps)
            //{
            //    if (emp.Address.ToLower().StartsWith(cityDetails.ToLower()))
            //    {
            //        searchResult.Add(emp);        
            //    }
            //}

            //if (searchResult.Count >0)
            //{
            //    foreach (Emp emp in searchResult)
            //    {
            //        Console.WriteLine("Name={0}, Address={1}", emp.Name, emp.Address);
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("No Match Found!!!");
            //} 
            #endregion

            #region Normal LINQ Query
            //List<Emp> emps = GetAllEmployees();

            //Console.WriteLine("Enter City Details to Find the Employee:");

            //cityDetails = Console.ReadLine();

            ////var searchResult = (from emp in emps
            ////                    where emp.Address.ToLower().StartsWith(cityDetails.ToLower())
            ////                    select emp).ToList();

            //if (searchResult.Count > 0)
            //{
            //    foreach (Emp emp in searchResult)
            //    {
            //        Console.WriteLine("Name={0}, Address={1}", emp.Name, emp.Address);
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("No Match Found!!!");
            //}

            #endregion

            #region Understanding "Where"  is a method
            //Way 1
            ////Func<Emp, bool> pointer = new Func<Emp, bool>(IsEmpWithSpecificCity);

            //Way 2
            ////Func<Emp, bool> pointer = delegate (Emp emp)
            ////                                {
            ////                                    return emp.Address.ToLower().StartsWith(cityDetails.ToLower());
            ////                                };

            //Way 3
            ////Func<Emp, bool> pointer = (emp)=>
            ////                                {
            ////                                    return emp.Address.ToLower().StartsWith(cityDetails.ToLower());
            ////                                };



            //List<Emp> emps = GetAllEmployees();

            //Console.WriteLine("Enter City Details to Find the Employee:");

            //cityDetails = Console.ReadLine();

            //This is where at below line we are using Way1, Way2 or Way3..please uncomment Way 1, 2, 3 one at a time while in lab

            ////var searchResult = emps.Where(pointer).ToList();


            //Way 4 - different try out
            //var searchResult = emps.Where((emp) =>
            //                                {
            //                                    return emp.Address.ToLower().StartsWith(cityDetails.ToLower());
            //                                }).ToList();


            //if (searchResult.Count > 0)
            //{
            //    foreach (Emp emp in searchResult)
            //    {
            //        Console.WriteLine("Name={0}, Address={1}", emp.Name, emp.Address);
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("No Match Found!!!");
            //}

            #endregion

            #region Storing Filtered Data inside Anonymous Type Collection

            //List<Emp> emps = GetAllEmployees();

            //Console.WriteLine("Enter City Details to Find the Employee:");

            //cityDetails = Console.ReadLine();


            //var searchResult = (from emp in emps
            //                    where emp.Address.ToLower().StartsWith(cityDetails.ToLower())
            //                    select new { EName = "Mr / Mrs " + emp.Name, ECity = emp.Address }).ToList();

            //foreach (var resultHolder in searchResult)
            //{
            //    Console.WriteLine(resultHolder.EName + " | " + resultHolder.ECity);
            //}


            ////var searchResult = (from emp in emps
            ////                    where emp.Address.ToLower().StartsWith(cityDetails.ToLower())
            ////                    select new ResultHolder() { EName = "Mr / Mrs " +  emp.Name, ECity = emp.Address }).ToList();

            ////foreach (ResultHolder resultHolder in searchResult)
            ////{
            ////    Console.WriteLine(resultHolder.EName + " | " + resultHolder.ECity);
            ////}
            #endregion

            #region Differed /Lazy Execution

            //List<Emp> emps = GetAllEmployees();

            //Console.WriteLine("Enter City Details to Find the Employee:");

            //cityDetails = Console.ReadLine();   //Considering city name starts with "K"

            //var searchResult = (from emp in emps
            //                    where emp.Address.ToLower().StartsWith(cityDetails.ToLower())
            //                    select emp);   //........Try adding .ToList();n see the result

            //emps.Add(new Emp() { No  = 99, Name = "mahesh", Address="Kolhapur" });

            //foreach (var emp in searchResult)
            //{
            //    Console.WriteLine(emp.Name);
            //}

            #endregion

        }


        public static bool IsEmpWithSpecificCity(Emp emp)
        {
            return emp.Address.ToLower().StartsWith(cityDetails.ToLower());
        }

        static List<Emp> GetAllEmployees()
        {
            //Here we will write logic to get Employee details from database....

            //As of now we will return hard coded data from this method.

            return new List<Emp>() {
              new Emp () {  No = 11, Name = "Rohit", Address = "Kolhapur"},
              new Emp () {  No = 12, Name = "Ajay", Address = "Solapur"},
              new Emp () {  No = 13, Name = "Nitin", Address = "Satana"},
              new Emp () {  No = 14, Name = "Harsh", Address = "Kashi"},
              new Emp () {  No = 15, Name = "Shekhar", Address = "Pune"},
              new Emp () {  No = 16, Name = "Niranjan", Address = "Panji"},
              new Emp () {  No = 17, Name = "Devdatta", Address = "Mumbai"}
            };
        }
    }
    public class Emp
    {
        public int No { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }

    public class ResultHolder
    {
        public string EName { get; set; }
        public string ECity { get; set; }
    }
}
