﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
namespace _01Demo
{
    //delegate int MyDelegate(int x, int y);
    class Program
    {
        static void Main(string[] args)
        {
            #region Normal Call
            //Stopwatch stopwatch = new Stopwatch();

            //stopwatch.Start();
            //int result = Add(10, 20);
            //stopwatch.Stop();

            //Console.WriteLine("Result is {0}", result);
            //Console.WriteLine("ElapsedTicks = {0}", stopwatch.ElapsedTicks);
            //Console.ReadLine();
            #endregion

            #region Using MyDelegate
            //MyDelegate pointer = new MyDelegate(Add);
            //Stopwatch stopwatch = new Stopwatch();

            //stopwatch.Start();
            //int result = pointer(10, 20);
            //stopwatch.Stop();

            //Console.WriteLine("Result is {0}", result);
            //Console.WriteLine("ElapsedTicks = {0}", stopwatch.ElapsedTicks);
            //Console.ReadLine(); 
            #endregion

            #region MyDelegate with Anonymous Method
            //MyDelegate pointer = delegate (int x, int y)
            //                        {
            //                            return x + y;
            //                        };

            //Stopwatch stopwatch = new Stopwatch();

            //stopwatch.Start();
            //int result = pointer(10, 20);
            //stopwatch.Stop();

            //Console.WriteLine("Result is {0}", result);
            //Console.WriteLine("ElapsedTicks = {0}", stopwatch.ElapsedTicks);
            //Console.ReadLine(); 
            #endregion

            #region MyDelegate with Lambada Expression
            //MyDelegate pointer =  (x, y) =>
            //                        {
            //                            return x + y;
            //                        };

            //Stopwatch stopwatch = new Stopwatch();

            //stopwatch.Start();
            //int result = pointer(10, 20);
            //stopwatch.Stop();

            //Console.WriteLine("Result is {0}", result);
            //Console.WriteLine("ElapsedTicks = {0}", stopwatch.ElapsedTicks);
            //Console.ReadLine(); 
            #endregion

            #region Lambada with Func
            //Func<int,int,int> pointer = (x, y) =>
            //                            {
            //                                return x + y;
            //                            };

            //Stopwatch stopwatch = new Stopwatch();

            //stopwatch.Start();
            //int result = pointer(10, 20);   //Creation of Tree -> Compilation of a Tree -> Execution
            //stopwatch.Stop();

            //Console.WriteLine("Result is {0}", result);
            //Console.WriteLine("ElapsedTicks = {0}", stopwatch.ElapsedTicks);
            //Console.ReadLine();
            #endregion

            #region Expressio  Tree
            //Expression< Func<int, int, int>> tree = (x, y) =>   x + y;  //Creation of a Expression Tree

            //Func<int, int, int> pointer = tree.Compile();            //Compilation of a Expression Tree

            //Stopwatch stopwatch = new Stopwatch();

            //stopwatch.Start();
            //int result = pointer(10, 20);   //Execution
            //stopwatch.Stop();

            //Console.WriteLine("Result is {0}", result);
            //Console.WriteLine("ElapsedTicks = {0}", stopwatch.ElapsedTicks);
            //Console.ReadLine();
            #endregion
        }



        //static int Add(int x, int y)
        //{
        //    return x + y;
        //}
    }
}
