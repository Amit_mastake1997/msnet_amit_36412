﻿
namespace Day14
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Objects;
    using System.Data.Objects.DataClasses;
    using System.Linq;
    
    public partial class Karad : DbContext
    {
        public Karad()
            : base("name=Karad")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<Emp> Emps { get; set; }
    
        public virtual ObjectResult<spGetUsersByAvoidingInjection_Result> spGetUsersByAvoidingInjection(string uname)
        {
            var unameParameter = uname != null ?
                new ObjectParameter("uname", uname) :
                new ObjectParameter("uname", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetUsersByAvoidingInjection_Result>("spGetUsersByAvoidingInjection", unameParameter);
        }
    
        public virtual int spInsertRecord(Nullable<int> no, string name, string adress)
        {
            var noParameter = no.HasValue ?
                new ObjectParameter("no", no) :
                new ObjectParameter("no", typeof(int));
    
            var nameParameter = name != null ?
                new ObjectParameter("name", name) :
                new ObjectParameter("name", typeof(string));
    
            var adressParameter = adress != null ?
                new ObjectParameter("adress", adress) :
                new ObjectParameter("adress", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("spInsertRecord", noParameter, nameParameter, adressParameter);
        }
    }
}
