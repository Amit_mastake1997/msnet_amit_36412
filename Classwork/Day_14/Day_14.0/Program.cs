﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Karad dbObj = new Karad();

            #region Select All using EF
            //var emps = dbObj.Emps.ToList();
            //foreach (var item in emps)
            //{
            //    Console.WriteLine(item.Name);
            //}

            #endregion

            #region Insert Using EF
            //Emp emp = new Emp();
            //emp.No = 55;
            //emp.Name = "Sachin";
            //emp.Address = "Mumbai";

            //dbObj.Emps.Add(emp);
            //dbObj.SaveChanges();
            #endregion

            #region Update using EF
            //var empToBeUpdated = (from emp in dbObj.Emps
            //                      where emp.No == 55
            //                      select emp).First();

            //empToBeUpdated.Name = "SachinT";
            //empToBeUpdated.Address = "Delhi";

            //dbObj.SaveChanges();
            #endregion

            #region Delete using EF
            //var empToBeDeleted = (from emp in dbObj.Emps
            //                      where emp.No == 55
            //                      select emp).First();

            //dbObj.Emps.Remove(empToBeDeleted);
            //dbObj.SaveChanges();
            #endregion

            #region Call Stored Procedure using EF
            //dbObj.spInsertRecord(44, "Amol", "Karad");
            #endregion

            Console.WriteLine("Done Changes");


        }
    }
}
