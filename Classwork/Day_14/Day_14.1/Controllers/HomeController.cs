﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _01Demo.Models;

namespace Day_14.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Emp emp1 = new Emp() { No = 1, Name = "Mahesh", Address = "Pune" };

            return View("xyz", emp1);
        }

        public ActionResult Show ()
        {
            
            Karad dbObj = new Karad();
            var emps = dbObj.Emps.ToList();
            return View("display", emps);


        }
    }
}