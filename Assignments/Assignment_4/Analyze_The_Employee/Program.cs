﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Analyze_The_Employee
{
    class Program
    {
        static void Main(string[] args)
        {

            Dictionary<int, Department> DeptList = new Dictionary<int, Department>();
            List<Employee> EmpList = new List<Employee>();

            #region Reading Department csv

            FileStream fs = new FileStream(@"E:\Assignment4\Assignment_4\Analyze_The_Employee\dept.csv", FileMode.Open, FileAccess.Read);

            StreamReader reader = new StreamReader(fs);
            string deptstring;

            while ((deptstring = reader.ReadLine()) != null)
            {
                string[] deptDetails = deptstring.Split(',');
                Department dept = new Department();
                dept.DeptNo = int.Parse(deptDetails[0]);
                dept.DeptName = deptDetails[1];
                dept.Location = deptDetails[2];
                DeptList.Add(dept.DeptNo, dept);
            }
            reader.Close();
            fs.Close();

            #endregion

            #region Reading Employee csv and Adding in respective Department


            FileStream fd = new FileStream(@"E:\Assignment4\Assignment_4\Analyze_The_Employee\emp.csv", FileMode.Open, FileAccess.Read);

            StreamReader readerEmp = new StreamReader(fd);
            string empstring;

            while ((empstring = readerEmp.ReadLine()) != null)
            {
                string[] empDetails = empstring.Split(',');
                Employee emp = new Employee();
                emp.EmpNo = int.Parse(empDetails[0]);
                emp.Name = empDetails[1];
                emp.Designation = empDetails[2];
                emp.Salary = float.Parse(empDetails[3]);
                emp.Comission = float.Parse(empDetails[4]);
                emp.DeptNo = int.Parse(empDetails[5]);
                EmpList.Add(emp);
            }
            readerEmp.Close();
            fd.Close();

            foreach (int keys in DeptList.Keys)
            {
                foreach (Employee employee in EmpList)
                {
                    if (keys == employee.DeptNo)
                    {
                        DeptList[keys].EMPList.Add(employee);
                    }
                }
            }
            //Console.ReadLine();
            #endregion


            while (true)
            {
                Console.WriteLine("Select your choice   :   \n1:Calculate Total Salary of All Employees" +
                    "\n2:Display All Employees of Department\n3:Display Departmentwise Count of Employees" +
                    "\n4:Display Departmentwise Average Salary of Employees" +
                    "\n5:Display Departmentwise Minimum Salary of Employees");

                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        #region Calculate Total Salary

                        double TotalSalary = 0;
                        foreach (Employee emp in EmpList)
                        {
                            TotalSalary += emp.CalculateTotalSalary();
                        }
                        Console.WriteLine(string.Format("Total Salary of all Employees  :   {0}", TotalSalary));

                        #endregion
                        break;
                    case 2:
                        #region Get All Emplyees By Department

                        Console.WriteLine("Enter department number  :   ");
                        int deptno = Convert.ToInt32(Console.ReadLine());
                        List<Employee> EmpListByDept = Department.GetEmployeesByDept(deptno, EmpList);
                        Console.WriteLine(string.Format("Employees of Department Number {0} are : ", deptno));
                        foreach (Employee obj in EmpListByDept)
                        {
                            Console.WriteLine(obj.Name);
                        }

                        #endregion
                        break;
                    case 3:
                        #region Departmentwise count of Employees

                        Dictionary<int, double> EmployeeCount = Department.DepartmentwiseStaffCount(DeptList);
                        double EmpCountperDepartment;
                        foreach (int keys in EmployeeCount.Keys)
                        {
                            EmpCountperDepartment = EmployeeCount[keys];
                            Console.WriteLine(string.Format("Department Number {0} has {1} Employees", keys, EmpCountperDepartment));
                        }

                        #endregion
                        break;
                    case 4:
                        #region Departmentwise Average Salary

                        Dictionary<int, double> AvgSalaryofEmployee = Department.DepartmentwiseAvgSalary(DeptList);
                        foreach (int keys in AvgSalaryofEmployee.Keys)
                        {
                            Console.WriteLine("Average salary of Department Number {0} is {1}", keys, AvgSalaryofEmployee[keys]);
                        }
                        #endregion
                        break;
                    case 5:
                        #region Departmentwise Minimum Salary

                        Dictionary<int, double> MinSalaryofEmployee = Department.DepartmentwiseMinSalary(DeptList);
                        foreach (int keys in MinSalaryofEmployee.Keys)
                        {
                            Console.WriteLine("Minimum salary of Department Number {0} is {1}", keys, MinSalaryofEmployee[keys]);
                        }
                        #endregion
                        break;
                    default:
                        Console.WriteLine("Choice is Incorrect...!");
                        break;
                }
                Console.WriteLine("You want to continue");
                string reply = Console.ReadLine();
                if (reply != "yes")
                {
                    break;
                }
            }

        }
    }

    public class Employee
    {
        private int _EmpNo;
        private string _Name;
        private string _Designation;
        private float _Salary;
        private float _Comission;
        private int _DeptNo;

        public Employee()
        {
            this.EmpNo = 0;
            this.Name = null;
            this.Designation = null;
            this.Salary = 0f;
            this.Comission = 0f;
            this.DeptNo = 0;
        }

        public Employee(int EmpNo, string Name, string Designation, float Salary, float Comission, int DeptNo)
        {
            this.EmpNo = EmpNo;
            this.Name = Name;
            this.Designation = Designation;
            this.Salary = Salary;
            this.Comission = Comission;
            this.DeptNo = DeptNo;
        }

        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }

        public float Comission
        {
            get { return _Comission; }
            set { _Comission = value; }
        }

        public float Salary
        {
            get { return _Salary; }
            set { _Salary = value; }
        }

        public string Designation
        {
            get { return _Designation; }
            set { _Designation = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int EmpNo
        {
            get { return _EmpNo; }
            set { _EmpNo = value; }
        }

        public override string ToString()
        {
            return string.Format("Emplyee   :   Employee No = {0}, Name = {1}, Designation = {2}, Salary = {3}, Commission = {4}",
                                this.EmpNo, this.Name, this.Designation, this.Salary, this.Comission);
        }

        public double CalculateTotalSalary()
        {
            double TotalSalary;
            TotalSalary = this.Salary + this.Comission;
            return TotalSalary;
        }
    }

    public class Department
    {
        private int _DeptNo;
        private string _DeptName;
        private string _Location;
        private List<Employee> _EMPList = new List<Employee>();

        public List<Employee> EMPList
        {
            get { return _EMPList; }
            set { _EMPList = value; }
        }


        public Department()
        {
            this.DeptNo = 0;
            this.DeptName = null;
            this.Location = null;
        }

        public Department(int DeptNo, string DeptName, string Location)
        {
            this.DeptNo = DeptNo;
            this.DeptName = DeptName;
            this.Location = Location;
        }

        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }

        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }

        public override string ToString()
        {
            return string.Format("Department    :   Department No = {0}, Department Name = {1}, Location = {2}",
                                this.DeptNo, this.DeptName, this.Location);
        }

        public static List<Employee> GetEmployeesByDept(int dno, List<Employee> EmpList)
        {
            List<Employee> emp1 = new List<Employee>();
            foreach (Employee e1 in EmpList)
            {
                if (dno == e1.DeptNo)
                {
                    emp1.Add(e1);
                }
            }
            return emp1;
        }

        public static Dictionary<int, double> DepartmentwiseStaffCount(Dictionary<int, Department> DeptList)
        {
            double CountofEmployees;
            Dictionary<int, double> EmpCountperDept = new Dictionary<int, double>();
            foreach (int keys in DeptList.Keys)
            {
                CountofEmployees = DeptList[keys].EMPList.Count;
                EmpCountperDept.Add(keys, CountofEmployees);
            }
            return EmpCountperDept;
        }

        public static Dictionary<int, double> DepartmentwiseAvgSalary(Dictionary<int, Department> DeptList)
        {
            double SalofEmployees = 0, AvgSalaryofEmployee;
            Dictionary<int, double> SalofEmps = new Dictionary<int, double>();
            foreach (int keys in DeptList.Keys)
            {
                if ((DeptList[keys].EMPList.Any()))
                {
                    foreach (Employee e in DeptList[keys].EMPList)
                    {
                        SalofEmployees += e.Salary;
                    }
                }
                else
                {
                    SalofEmployees = 0;
                }
                if (SalofEmployees != 0)
                {
                    AvgSalaryofEmployee = SalofEmployees / DeptList[keys].EMPList.Count;
                    SalofEmps.Add(keys, AvgSalaryofEmployee);
                }
                else
                {
                    SalofEmps.Add(keys, 0);
                }

            }
            return SalofEmps;
        }

        public static Dictionary<int, double> DepartmentwiseMinSalary(Dictionary<int, Department> DeptList)
        {
            double MinSalofEmployee = double.MaxValue;
            Dictionary<int, double> SalofEmps = new Dictionary<int, double>();

            foreach (int keys in DeptList.Keys)
            {
                if (DeptList[keys].EMPList.Any())
                {
                    foreach (Employee e in DeptList[keys].EMPList)
                    {
                        if ((e.Salary < MinSalofEmployee))
                        {
                            MinSalofEmployee = e.Salary;
                        }
                    }
                }
                else
                {
                    MinSalofEmployee = 0;
                }
                SalofEmps.Add(keys, MinSalofEmployee);
                MinSalofEmployee = double.MaxValue;
            }
            return SalofEmps;
        }

    }
}
