﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment_6._1.Models;

namespace Assignment_6._1.Controllers
{
    public class DeptController : Controller
    {
        
        Hrdb dbobj = new Hrdb();

        public ActionResult Index()
        {
            var depts = dbobj.Departments.ToList();
            return View(depts);
        }
        [HttpGet]
       
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
    
        public ActionResult Create(Department dept)
        {
            dbobj.Departments.Add(dept);
            dbobj.SaveChanges();
            return Redirect("/Dept/ListDept");
        }

        public ActionResult AfterCreate(FormCollection entireFormData)
        {
            int id = Convert.ToInt32(entireFormData["DeptNo"]);
            string name = entireFormData["DeptName"].ToString();
            string address = entireFormData["Location"].ToString();

            Department department = new Department() { DeptNo = id, DeptName = name, Location = address };

            dbobj.Departments.Add(department);

            dbobj.SaveChanges();

            return Redirect("/Dept/ListDept");
        }

        public ActionResult Update(int id)
        {

            var deptToBeUpdated = (from dept in dbobj.Departments
                                   where dept.DeptNo == id
                                   select dept).First();
            return View(deptToBeUpdated);
        }

        public ActionResult AfterUpdate(Department deptUpdated)
        {
            var deptToBeUpdated = (from dept in dbobj.Departments
                                   where dept.DeptNo == deptUpdated.DeptNo
                                   select dept).First();
            deptToBeUpdated.DeptName = deptUpdated.DeptName;
            deptToBeUpdated.Location = deptUpdated.Location;

            dbobj.SaveChanges();

            return Redirect("/Dept/ListDept");
        }

        public ActionResult Delete(int id)
        {

            var deptToBeUpdated = (from dept in dbobj.Departments
                                   where dept.DeptNo == id
                                   select dept).First();

            dbobj.Departments.Remove(deptToBeUpdated);
            dbobj.SaveChanges();

            return Redirect("/Dept/ListDept");
        }

        public ActionResult ListDept()
        {

            var depts = dbobj.Departments.ToList();
            return View(depts);
        }

        public ActionResult Display()
        {
            var depts = dbobj.Departments.ToList();
            return View(depts);
        }
    }
}