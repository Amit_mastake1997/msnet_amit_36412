﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment_6._2.Utility;

namespace Assignment_6._2.Filters
{
    public class LogFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Logger.CurrentLogger.Log(
                                string.Format("{0}/{1} is about to start execution",
                                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                                filterContext.ActionDescriptor.ActionName));
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Logger.CurrentLogger.Log(
                                 string.Format("{0}/{1} is execution completed",
                                 filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                                 filterContext.ActionDescriptor.ActionName));
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Logger.CurrentLogger.Log("UI is getting processed");
        }
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            Logger.CurrentLogger.Log("UI processed!!");
        }
    }
}