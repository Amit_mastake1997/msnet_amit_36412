﻿using Assignment_6._2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;


namespace Assignment_6._2.Controllers
{
    public class DashboardController : Controller
    {
        Hrdb dbobj = new Hrdb();
       
        [AllowAnonymous]
        public ActionResult Index(string email)
        {
            Employee empupdate = new Employee();

            var temp = (from emp in dbobj.Employees where emp.Email == email select emp).First();
            empupdate.EmpNo = temp.EmpNo;
            empupdate.Name = temp.Name;
            empupdate.Designation = temp.Designation;
            empupdate.HireDate = Convert.ToDateTime(temp.HireDate);
            empupdate.Salary = temp.Salary;
            empupdate.Commision = temp.Commision;
            empupdate.Mobileno = Convert.ToInt32(temp.Mobileno);
            empupdate.Email = temp.Email;
            empupdate.Passcode = temp.Passcode;
            empupdate.DeptNo = Convert.ToInt32(temp.DeptNo);

            return View(empupdate);


            //ViewBag.Email = User.Identity.Name;
            //var employees = dbobj.Employees.ToList();
            //return View(employees);
        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return Redirect("/User/SignIn");
        }
    }
}