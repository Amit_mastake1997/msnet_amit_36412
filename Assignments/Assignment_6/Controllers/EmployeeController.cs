﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment_6._1.Models;

namespace Assignment_6._1.Controllers
{
    public class EmployeeController : Controller
    {
       


        Hrdb dbobj = new Hrdb();
        public ActionResult Index()
        {
            var employees = dbobj.Employees.ToList();
            return View(employees);
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            dbobj.Employees.Add(employee);
            dbobj.SaveChanges();
            return Redirect("/Employee/ListEmployee");
        }


        public ActionResult AfterCreate(FormCollection entireFormData)
        {
            int id = Convert.ToInt32(entireFormData["EmpNo"]);
            string name = entireFormData["Name"].ToString();
            string email = entireFormData["Email"].ToString();
            string passcode = entireFormData["Passcode"].ToString();
            string designation= entireFormData["Designation"].ToString();
            double salary = Convert.ToDouble(entireFormData["Salary"]);
            int mobileno = Convert.ToInt32(entireFormData["Mobileno"]);
            double commistion = Convert.ToDouble(entireFormData["Commistion"]);
            DateTime dateTime = Convert.ToDateTime(entireFormData["HireDate"]);
            int deptno = Convert.ToInt32(entireFormData["DeptNo"]);

            Employee employee = new Employee() { EmpNo = id, Name = name, Email = email,Passcode=passcode,Salary=salary,Commision=commistion,Designation=designation,Mobileno=mobileno,HireDate=dateTime,DeptNo=deptno };

            dbobj.Employees.Add(employee);

            dbobj.SaveChanges();

            return Redirect("/Employee/ListEmployee");
        }

        public ActionResult Update(int id)
        {
            var employeeToBeUpdated = (from Employee in dbobj.Employees where Employee.EmpNo == id select Employee).First();

            return View(employeeToBeUpdated);
        }


        public ActionResult AfterUpdate(Employee empUpdated)
        {
            var employeeToBeUpdated = (from Employee in dbobj.Employees
                                       where Employee.EmpNo == empUpdated.EmpNo
                                       select Employee).First();
            employeeToBeUpdated.Name = empUpdated.Name;
            employeeToBeUpdated.Email = empUpdated.Email;
            employeeToBeUpdated.Passcode = empUpdated.Passcode;
            employeeToBeUpdated.Salary = empUpdated.Salary;
            employeeToBeUpdated.Commision = empUpdated.Commision;
            employeeToBeUpdated.Mobileno = empUpdated.Mobileno;
            employeeToBeUpdated.Designation = empUpdated.Designation;
            employeeToBeUpdated.HireDate = empUpdated.HireDate;
            employeeToBeUpdated.DeptNo = empUpdated.DeptNo;
            dbobj.SaveChanges();
            return Redirect("/Employee/ListEmployee");

        }

        public ActionResult Delete(int id)
        {
            var employeeToBeDeleted = (from Employee in dbobj.Employees where Employee.EmpNo == id select Employee).First();

            dbobj.Employees.Remove(employeeToBeDeleted);
            dbobj.SaveChanges();
            return Redirect("/Employee/ListEmployee");
        }


        public ActionResult ListEmployee()
        {
            var employees = dbobj.Employees.ToList();
            return View(employees);
        }

        public ActionResult Display()
        {
            var employees = dbobj.Employees.ToList();
            return View(employees);
        }
    }
}