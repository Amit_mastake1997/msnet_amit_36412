﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;

namespace Assignment_6._3.Uitility
{

    public class Logger
    {
        private static Logger logger = new Logger();
        FileStream fs = null;
        StreamWriter writer = null;
        string FilePath = null;
        private Logger() { }

        private void Initialize()
        {
            this.FilePath = ConfigurationManager.AppSettings["LogFile"].ToString();

            if (File.Exists(FilePath))
            {
                fs = new FileStream(FilePath, FileMode.Append, FileAccess.Write);
            }
            else
            {
                fs = new FileStream(FilePath, FileMode.OpenOrCreate, FileAccess.Write);
            }
            writer = new StreamWriter(fs);
        }

        private void Clear()
        {
            writer.Close();
            fs.Close();

            writer = null;
            fs = null;
        }

        public static Logger CurrentLogger
        {
            get { return logger; }
        }

        public void Log(string message)
        {
            Initialize();
            writer.WriteLine(string.Format("Logged: {0} at {1}", message, DateTime.Now.ToString()));
            Clear();
        }


    }
}