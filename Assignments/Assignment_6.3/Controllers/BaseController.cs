﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Assignment_6._3.Filters;

namespace Assignment_6._3.Controllers
{
    [LogFilter]
    [Authorize]
    [HandleError(ExceptionType = typeof(SqlException), View = "myerror")]
    [HandleError(ExceptionType = typeof(Exception), View = "myerror")]
    public class BaseController : Controller
    {

    }
}