﻿using Assignment_6._3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_6._3.Controllers
{
    public class TryOutsController : Controller
    {
        // GET: TryOuts
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSomething(int id)
        {
            ActionResult result = null;
            switch (id)
            {
                case 1:
                    result = View("Demo");
                    break;
                case 2:
                    Hrdb dbobj = new Hrdb();
                    var allEmpInList = dbobj.Employees.ToList();
                    result = new JsonResult()
                    {
                        Data = allEmpInList,
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                    break;
                case 3:
                    result = new ContentResult()
                    {
                        Content = "This is some text ",
                        ContentType = "text/plain"
                    };
                    break;
                case 4:
                    result = new JavaScriptResult() { Script = "<script> alert ('Hi')</script>" };
                    break;
                default:
                    result = null;
                    break;
            }

            return result;
        }
    }
}
