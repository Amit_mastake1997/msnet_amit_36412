﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment_6._3.Models;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.Net.Mail;


namespace Assignment_6._3.Controllers
{
    public class EmployeeController : BaseController
    {

        Hrdb dbobj = new Hrdb();

        [AllowAnonymous]
        public ActionResult IsEmailTaken(string id)
        {
            var empdFound = (from Employee in dbobj.Employees.ToList()
                             where Employee.Email == id
                             select Employee).ToList();

            if (empdFound.Count > 0)
            {
                return new JsonResult() { Data = true, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            else
            {
                return new JsonResult() { Data = false, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [Authorize]
        public ActionResult Index()
        {
            var employees = dbobj.Employees.ToList();
            return View(employees);
        }

      
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            dbobj.Employees.Add(employee);
            dbobj.SaveChanges();
            return Redirect("/Employee/ListEmployee");
        }



        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        
        public ActionResult AfterCreate(FormCollection entireContactForm)
        {
            Employee emp = new Employee();

            emp.EmpNo = Convert.ToInt32(entireContactForm["EmpNo"]);
            emp.Name = entireContactForm["name"].ToString();
            emp.Email = entireContactForm["email"].ToString();
            emp.Passcode = entireContactForm["passcode"].ToString();
            emp.Salary = Convert.ToInt32(entireContactForm["salary"]);
            emp.Commision = Convert.ToDouble(entireContactForm["commision"]);
            emp.Designation = entireContactForm["Designation"].ToString();
            //emp.Hiredate = Convert.ToDateTime(entireContactForm["Hiredate"]);
            emp.HireDate = DateTime.Parse(entireContactForm["Hiredate"]);
            emp.Mobileno = Convert.ToInt32(entireContactForm["mobileno"]);
            emp.Passcode = entireContactForm["Passcode"].ToString();
            emp.DeptNo = Convert.ToInt32(entireContactForm["DeptNo"]);
            dbobj.Employees.Add(emp);
            dbobj.SaveChanges();
            
            string name = entireContactForm["name"].ToString();
            string email = entireContactForm["email"].ToString();
            string query = entireContactForm["query"].ToString();
            

            string adminEMailID = ConfigurationManager.AppSettings["email"].ToString();
            string adminPassword = ConfigurationManager.AppSettings["password"].ToString();

            string smtpDetails = "smtp.gmail.com";
            int smtpPort = 587;

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(adminEMailID);
            mail.To.Add(email);
            mail.CC.Add(adminEMailID);
            mail.Subject = "You are hired";
            mail.Body = string.Format("<h3> To {0} <br/>{1}<h3> <h4> <br/><br/> Dear {0}, <br/><br/> We glad to inform you are successful for the position of a {2}. Your skills and experience meet the requirements for this position. <br/> You should start work on {3}. <br/><br/> It’s a full-time position, and you will report to BOSS. Company, is offering an annual salary of {4} per annum, payable monthly. <br/><br/> Sincerely, <br/>HR </h4>", emp.Name, query, emp.Designation, emp.HireDate, emp.Salary);


            mail.IsBodyHtml = true;
        

            SmtpClient smtp = new SmtpClient(smtpDetails, smtpPort);
            smtp.Credentials = new NetworkCredential(adminEMailID, adminPassword);
            smtp.EnableSsl = true;
            smtp.Send(mail);
            return Redirect("/Employee/ListEmployee");

        }


        public ActionResult Update(int id)
        {
            var employeeToBeUpdated = (from Employee in dbobj.Employees where Employee.EmpNo == id select Employee).First();

            return View(employeeToBeUpdated);
        }

        [ValidateAntiForgeryToken]
        public ActionResult AfterUpdate(Employee empUpdated)
        {
            var employeeToBeUpdated = (from Employee in dbobj.Employees
                                       where Employee.EmpNo == empUpdated.EmpNo
                                       select Employee).First();
            employeeToBeUpdated.Name = empUpdated.Name;
            employeeToBeUpdated.Email = empUpdated.Email;
            employeeToBeUpdated.Passcode = empUpdated.Passcode;
            employeeToBeUpdated.Salary = empUpdated.Salary;
            employeeToBeUpdated.Commision = empUpdated.Commision;
            employeeToBeUpdated.Mobileno = empUpdated.Mobileno;
            employeeToBeUpdated.Designation = empUpdated.Designation;
            employeeToBeUpdated.HireDate = empUpdated.HireDate;
            employeeToBeUpdated.DeptNo = empUpdated.DeptNo;
            dbobj.SaveChanges();
            return Redirect("/Employee/ListEmployee");

        }

        public ActionResult Delete(int id)
        {
            var employeeToBeDeleted = (from Employee in dbobj.Employees where Employee.EmpNo == id select Employee).First();

            dbobj.Employees.Remove(employeeToBeDeleted);
            dbobj.SaveChanges();
            return Redirect("/Employee/ListEmployee");
        }

        [AllowAnonymous]
        public ActionResult ListEmployee()
        {
            ViewBag.Email = User.Identity.Name;
            var employees = dbobj.Employees.ToList();
            return View(employees);
        }

        public ActionResult Display()
        {
            var employees = dbobj.Employees.ToList();
            return View(employees);
        }
    }
}