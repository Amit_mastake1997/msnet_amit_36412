﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Data.SqlClient;
using Assignment_6._3.Models;

namespace Assignment_6._3.Controllers
{
    public class UserController : Controller
    {

        public ActionResult SignIn()
        {

            return View();
        }

        [HttpPost]
        public ActionResult SignIn(Employee loggedInEmployee, string ReturnUrl)
        {
            //string email = loggedInEmployee.Email;

            if (this.CheckEmployeeInDB(loggedInEmployee))
            {

                FormsAuthentication.SetAuthCookie(loggedInEmployee.Email, false);

                if (ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    //return Redirect(string.Format("/Dashboard/Index/?email={0}", email));
                     return Redirect("/Dashboard/Index");
                }
            }
            else
            {
                ViewBag.ErrorMessage = "Email / Password is Wrong";
                return View();
            }

        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return Redirect("/User/SignIn");
        }

        private bool CheckEmployeeInDB(Employee loggedInEmployee)
        {
            Hrdb dbobj = new Hrdb();

            var emps = dbobj.Employees.ToList();
            foreach (var item in emps)
            {
                if (loggedInEmployee.Email == item.Email && loggedInEmployee.Passcode == item.Passcode)
                {
                    return (loggedInEmployee.Email == item.Email && loggedInEmployee.Passcode == item.Passcode);
                }
            }
            return false;
            //return (loggedInEmployee.Email == "manali@gmail.com" && loggedInEmployee.Passcode == "manali@123");
        }

    }
}