﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Assignment_3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n1: Write Into File\n2:Read From File ");
            int choice = Convert.ToInt32(Console.ReadLine());

            if (choice==1)
            {
               
                FileStream fss = new FileStream(@"C:\Users\om\Desktop\msnet\msnet_manali_36436\Assignments\Assignment_3\Assignment_3.1\Product.txt",
                                                    FileMode.OpenOrCreate,
                                                    FileAccess.Write);

                BinaryFormatter writer = new BinaryFormatter();

                    Product p = new Product();

                    Console.WriteLine("Enter Title:");
                    p.Title = Console.ReadLine();


                    Console.WriteLine("Enter Price:");
                    p.Price = Convert.ToInt32(Console.ReadLine());



                    Console.WriteLine("Enter Category:");
                    p.Category = Console.ReadLine();


                    Console.WriteLine("Enter Manufacturer:");
                    p.Manufacture = Console.ReadLine();


                    writer.Serialize(fss, p);


                    fss.Flush();

                    writer = null;
                    fss.Close();

                }

                else
                {
                 
                    FileStream fs = new FileStream(@"C:\Users\om\Desktop\msnet\msnet_manali_36436\Assignments\Assignment_3\Assignment_3.1\Product.txt", FileMode.Open, FileAccess.Read);

                    BinaryFormatter reader = new BinaryFormatter();

                    Product someEmployeeObject = (Product)reader.Deserialize(fs);

                    Console.WriteLine(someEmployeeObject.Title);
                Console.WriteLine(someEmployeeObject.Price);
                Console.WriteLine(someEmployeeObject.Category);
                Console.WriteLine(someEmployeeObject.Manufacture);

                reader = null;
                    fs.Close();
                    
                }
            }
        }
    

    [Serializable]
    public class Product
    {
        #region PrivateMemorys

        [NonSerialized] private string product_code = "abc@123";
       
        
        private string title;
        private float price;
        private string category;
        private string _Manufacture;
      

        #endregion

        #region Constructors
        public Product()
        {
            this.title = "";
            this.price = 0;
            this.category = "";
            this._Manufacture = "";
           
        }

        public Product(string title, float price,string category,string _Manufacture)
        {
            this.title = title;
            this.price = price;
            this.category = category;
            this._Manufacture = _Manufacture;
            
        }
        #endregion


        #region Properties
        public string Manufacture
        {
            get { return _Manufacture; }
            set { _Manufacture = value; }
        }


        public string Category
        {
            get { return category; }
            set { category = value; }
        }



        public float Price
        {
            get { return price; }
            set { price = value; }
        }


        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        #endregion

    }

}
