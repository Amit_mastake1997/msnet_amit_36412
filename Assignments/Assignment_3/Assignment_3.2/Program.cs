﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_3._2
{
    class Program
    {
        static void Main(string[] args)
        {

            #region WritetoFile
            FileStream fs = new FileStream(@"C:\Users\om\Desktop\msnet\msnet_manali_36436\Assignments\Assignment_3\Assignment_3.2\Product.txt", FileMode.OpenOrCreate, FileAccess.Write);
            Product p = new Product();

            BinaryFormatter writer = new BinaryFormatter();

            p.acceptDetails();

            writer.Serialize(fs, p);

            writer = null;
            fs.Close();

            Logger.CurrentLogger.Log(p.title + ", " + "Product written to file" + ", " + DateTime.Now.ToString());
            #endregion

            #region ReadFromFile
            FileStream fs1 = new FileStream(@"C:\Users\om\Desktop\msnet\msnet_manali_36436\Assignments\Assignment_3\Assignment_3.2\Product.txt", FileMode.Open, FileAccess.Read);
            BinaryFormatter reader = new BinaryFormatter();
            Product someProduct = (Product)reader.Deserialize(fs1);
            Console.WriteLine(someProduct.title);

            reader = null;
            fs1.Close();
            Logger.CurrentLogger.Log(someProduct.title + ", " + "Product Details Readed from file" + ", " + DateTime.Now.ToString());

            #endregion

            // Logger.CurrentLogger.readLog();



        }
    }
    public class Logger
    {
        private static Logger logger = new Logger();
        #region Constructor
        private Logger()
        {

        }
        #endregion

        #region properties
        public static Logger CurrentLogger
        {
            get
            {
                return logger;
            }
        }

        public void Log(string message)
        {
            FileStream f = new FileStream(@"C:\Users\om\Desktop\msnet\msnet_manali_36436\Assignments\Assignment_3\Assignment_3.2\Log.txt", FileMode.Append, FileAccess.Write);
            StreamWriter writer = new StreamWriter(f);

            writer.WriteLine(message);
            writer.Flush();
            f.Flush();
            writer.Close();
            f.Close();
        }

        public void readLog()
        {
            FileStream f1 = new FileStream(@"C:\Users\om\Desktop\msnet\msnet_manali_36436\Assignments\Assignment_3\Assignment_3.2\Log.txt", FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(f1);
            string allDataFromFile = reader.ReadToEnd();
            Console.WriteLine(allDataFromFile);
            Console.ReadLine();
            reader.Close();
            f1.Close();

        }
        #endregion
    }

    [Serializable]
    class Product
    {
        #region Private member
        private string _title;
        private float _price;
        private string _category;
        private string _manufacturer;

        [NonSerialized]
        private string _productCode;
        #endregion

        #region constructors

        public Product()
        {
            this.title = "";
            this.price = 0;
            this.category = "";
            this.manufacturer = "";
            this.productCode = "";
        }

        public Product(string title, float price, string category, string manufacturer, string prodcode)
        {
            this.title = title;
            this.price = price;
            this.category = category;
            this.manufacturer = manufacturer;
            this.productCode = productCode;
        }

        #endregion

        #region Properties
        public string productCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        public string manufacturer
        {
            get { return _manufacturer; }
            set { _manufacturer = value; }
        }


        public string category
        {
            get { return _category; }
            set { _category = value; }
        }


        public float price
        {
            get { return _price; }
            set { _price = value; }
        }


        public string title
        {
            get { return _title; }
            set { _title = value; }
        }

        public void acceptDetails()
        {
            Console.WriteLine("Enter Title:");
            this.title = Console.ReadLine();
            Console.WriteLine("Enter Price:");
            this.price = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter category:");
            this.category = Console.ReadLine();
            Console.WriteLine("Enter manufacturer:");
            this.manufacturer = Console.ReadLine();
            Console.WriteLine("Enter Product code");
            this.productCode = Console.ReadLine();
        }


        public void readDetails()
        {
            Console.WriteLine("Product details are:" + "  " + this.title + " , " + this.price + " , " + this.category + ", " + this.manufacturer);
        }
        #endregion
    }

}