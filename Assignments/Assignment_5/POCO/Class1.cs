﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyAttributes;
	
	
namespace POCO
{
	[Table(TableName = "AuthorInfo")]
	public class Author
	{
		[Column(ColumnName = "AName", ColumnType = "varchar", ColumnSize = 50, ckey = KeyConstraints.PRIMARY_KEY)]
		public string Name { get; set; }

		[Column(ColumnName = "CreationTime", ColumnType = "DateTime", ckey = KeyConstraints.UNIQUE_KEY)]
		public DateTime dateTime { get; set; }

		[Column(ColumnName = "Description", ColumnType = "varchar", ColumnSize = 50, ckey = KeyConstraints.NOT_NULL)]
		public string Description { get; set; }



	}
}