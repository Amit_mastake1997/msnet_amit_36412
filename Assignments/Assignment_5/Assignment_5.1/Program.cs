﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using MyAttributes;

namespace Assignment_5._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter path:");
            string path = Console.ReadLine();

            Assembly assemblyInfo = Assembly.LoadFrom(path);
            Type[] alltypes = assemblyInfo.GetTypes();
            foreach (Type type in alltypes)
            {
                Console.WriteLine(type.Name);
                string Query = "CREATE TABLE ";
                IEnumerable<Attribute> allAttribute = type.GetCustomAttributes();
                foreach (Attribute attribute in allAttribute)
                {
                    if (attribute is Table)
                    {
                        Table details = (Table)attribute;
                        // Console.WriteLine(details.TableName);
                        Query = Query + details.TableName + " ( ";
                        break;
                    }
                }
                PropertyInfo[] allproperties = type.GetProperties();
                foreach (PropertyInfo property in allproperties)
                {
                    IEnumerable<Attribute> allpropertyAttributes = property.GetCustomAttributes();
                    foreach (Attribute attribute in allpropertyAttributes)
                    {
                        if (attribute is Column)
                        {
                            Column Cdetails = (Column)attribute;
                            // string hold = "Manali";
                            //  Console.WriteLine(Cdetails.ColumnName +" "+hold);
                            Console.WriteLine("// " + Cdetails.ColumnName + " " + Cdetails.ckey + " //");
                            Query = Query + Cdetails.ColumnName + " " + Cdetails.ckey;
                            Query = Query + " " + Cdetails.ColumnType;
                            if (Cdetails.ColumnType == "varchar")
                            {
                                Query = Query + "(" + Cdetails.ColumnSize.ToString() + " ) ";
                            }
                            Query = Query + " ,";
                            break;
                        }
                    }
                }

                Query = Query.TrimEnd(',');
                Query = Query + " ) ";
                Console.WriteLine(Query);
                Console.WriteLine(" {0} ", DateTime.Now);
                Console.ReadLine();
            }
        }
    }
}
