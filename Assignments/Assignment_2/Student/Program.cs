﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student
{

    class Student
    {

        private string name;
        private bool gender;
        private int age;
        private int std;
        private char div;
        private double marks;

        public Student()
        {
            name = "Manali";
            gender = true;
            age = 22;
            std = 1;
            div = 'A';
            marks = 98.00;
        }

        public Student(string name, bool gender, int age, int std, char div, double marks)
        {


            this.name = name;
            this.gender = gender;
            this.age = age;
            this.std = std;
            this.div = div;
            this.marks = marks;
        }
        public void acceptDetails(string name, bool gender, int age, int std, char div, double marks)
        {

            this.name = name;
            this.gender = gender;
            this.age = age;
            this.std = std;
            this.div = div;
            this.marks = marks;
        }


        public void printDetails()
        {

            Console.WriteLine("Student's Details : ");
            Console.WriteLine("\tName     : " + name);
            Console.WriteLine("Consider Male AS 'FALSE' and Female AS 'TRUE'");
            Console.WriteLine("\tgender   : " + gender);
            Console.WriteLine("\tage      : " + age);
            Console.WriteLine("\tstd    : " + std);
            Console.WriteLine("\tdiv   : " + div);
            Console.WriteLine("\tMarks      : " + marks);
        }

    }
    class Program
    {
        static void Main()
        {

            Student S1 = new Student();

            S1.printDetails();

            Student S2 = new Student("Sharddha", true, 24, 2, 'C', 98.12);
            S2.printDetails();
        }
    }
}
