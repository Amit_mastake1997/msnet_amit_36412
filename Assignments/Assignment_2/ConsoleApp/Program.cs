﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Student s1 = new Student();
            s1.printInfo();
            s1.AcceptDetails();
            s1.printInfo();
        }
    }

    public class Student
    {
        private string name;
        private bool gender;
        private int age;
        private int std;
        private char div;
        private double marks;

        public Student()
        {
            this.name = "Manali";
            this.gender = true;
            this.age = 22;
            this.std = 1;
            this.div = 'A';
            this.marks = 95.5;
        }

        public Student(string name,bool gender, int age, int std,char div,double marks)
        {
            this.name = name;
            this.gender = gender;
            this.age = age;
            this.std = std;
            this.div= div;
            this.marks = marks;

        }

        public void AcceptDetails()
        {
            Console.WriteLine("Enter Name:");
            Name = Console.ReadLine();
            Console.WriteLine("Enter Gentetr:");
            Gender = Convert.ToBoolean(Console.ReadLine());
            Console.WriteLine("Enter the Age:");
            Age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Std:");
            Std = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the Div:");
            Div= Convert.ToChar(Console.ReadLine());
            Console.WriteLine("Enter the Marks:");
            Marks = Convert.ToDouble(Console.ReadLine());

        }

        public void printInfo()
        {

            //Console.WriteLine(Name + "," + Gender + "," + Age+","+Std+","+Div+","+","+Marks);
            Console.WriteLine("student Name is: " + name);
            Console.WriteLine("Consider Male AS 'FALSE' and Female AS 'TRUE'");
            Console.WriteLine("student Gender is: " + gender);
            Console.WriteLine("student Age is: " + age);
            Console.WriteLine("student Standard is: " + std);
            Console.WriteLine("student Division is: " + div);
            Console.WriteLine("student Marks is: " + marks);
            Console.ReadLine();

        }

        public double Marks
        {
            get { return marks; }
            set { marks = value; }
        }

        public char Div
        {
            get { return div; }
            set { div = value; }
        }

        public int Std
        {
            get { return std; }
            set { std = value; }
        }

        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        public bool Gender
        {
            get { return gender; }
            set { gender = value; }
        }


        public string Name
        {
            get { return name; }
            set { name = value; }
        }

    }

}