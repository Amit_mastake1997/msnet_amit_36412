﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempratureNS
{
    public class Temprature
    {
        public double FarenheitToCelcius(double temp)
        {
            return (temp - 32) * 5 / 9;
        }

        public double CelciusToFarenheit(double temp)
        {
            return ((temp * 9) / 5 + 32);
        }
    }
}
