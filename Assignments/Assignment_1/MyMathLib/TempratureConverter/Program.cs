﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempratureNS;

namespace TempratureConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            Temprature temprature = new Temprature();
            int ch;
            do
            {
                Console.Write("Enter temperature : ");
                double temp = Convert.ToDouble(Console.ReadLine());


                Console.WriteLine("Enter Your Operation Choice");

                Console.WriteLine("1: FarenheitToCelcius, 2: CelciusToFarenheit,3:Exit");
                string choice = Console.ReadLine();
                ch = Convert.ToInt32(choice);

                switch (ch)
                {
                    case 1:
                        double FarenheitToCelcius;
                        FarenheitToCelcius = temprature.FarenheitToCelcius(temp);
                        Console.WriteLine("The converted Celsius temperature is " + FarenheitToCelcius);
                        break;

                    case 2:
                        double CelciusToFarenheit;
                        CelciusToFarenheit = temprature.CelciusToFarenheit(temp);
                        Console.WriteLine("The converted Farenheit temperature is " + CelciusToFarenheit);
                        break;
                    case 3:
                        Console.WriteLine("Thank You!");
                        break;
                    default:
                        Console.WriteLine("Invalid");
                        break;

                }
                // Console.WriteLine("Hit Enter to Continue Doing Operations...");
                //Console.ReadLine();

            } while (ch != 3);
           
        }
    }

    /*public class Temprature
    {
        public double FarenheitToCelcius(double temp)
        {
            return (temp - 32) * 5 / 9;
        }

        public double CelciusToFarenheit(double temp)
        {
            return ((temp * 9) / 5 + 32);
        }
    }*/
}
